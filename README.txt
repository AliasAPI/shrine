
SHRINE Project Description

shrine is a broker-neutral open source software system.  The design emphasizes simplicity, security, and stability.
This document covers the design, transmissions, and setup.  For more information contact use at doitnowpa @ gmail.com.

Currently, shrine has a testbroker (for papertrading) and also connects to Tradier and Etrade.  
Later, shrine will send signals to the open source Tradelink system to connect to IB, Lightspeed, Sterling, 
Hold / GrayBox, Blackwood, DAS, TD Ameritrade, MB Trading, REDI / Goldman, RealTick +, Rithmic +, DTN iqFeed, 
FIX ++, Takion, & Genesis.



SIMPLICITY:
Working with shrine is as easy as sending a request and getting a response.

Your server sends a $request
  . CURL POST 
  . Each request must contain an alias, timestamp, tracking id, and request directive.
  . with a username and password
  . in JSON
Your server receives $response in JSON.

There are 3 types of requests:
  1. 'get broker status' returns all the information your server needs to audit the brokerage account.
    1a. cash_broker is the amount of net cash available to spend
    1b. open_market indicates whether the market is currently open or closed
    1c. margin indicates whether the account type is margin (FINRA 4210) or cash (Regulation T)
    1d. broker_positions lists all the symbols and number of shares that the brokerage account owns
    1e. done_orders lists all the order fills for the current trading day
    1f. open_orders lists the number of pending shares for each symbol 
  2. 'get quotes' returns all information needed to buy and sell equities
    Currently, the "last traded price" is the quote returned for each symbol
  3. 'order' places buy and sell order
    The use of client order ids (unique order IDs sent by the client) is preferred.
    Order IDs are necessary, and LOT (fill) IDs are preferred but not required.
    Orders are placed fill_or_kill / all_or_none

  NOTE: It's easy to modify shrine to retrieve more or less information with these 3 types of requests.
  The Etrade broker does not return the open_market value. Shrine can send a request to itself to get 
  the open_market information from a Tradier broker account. Examples of the $requests and $responses
  are located at the bottom of this file.


SECURITY
  First, the code is designed to be deployed on Openshift servers (that are run by Redhat.com).
  At this time, OpenShift runs on Amazon's EC2 cloud and inherits the security features of that 
  platform.  The systems is hardened with technologies like:
    SELinux
    Process, network, and storage separation
    Statefull and stateless inspection firewall
    Proactive monitoring of capacity limits (CPU, disk, memory, etc.)
    Intrusion detection (files, ports, back doors, etc.)
    Port monitoring
    Pam namespace
    Security compliance frameworks
    RPM verification and vulnerabilities updated
    Remote logging
    Encrypted communications (SSH, SSL, etc.)
    Risk assessment and security consultation is provided by Red Hat's Product Security Team.
    Red Hat's Product Security Team helps identify and prevent new exploits. This team frequently 
    tests exploits such as cross site scripting (XSS) and that cookie permissions are set appropriately.

  All $request and $response transmissions use an alias. The alias is an anonymous code name for the account.
  The sensitive information (like the account keys and secret) are stored in the /shrine/config.php file.
  All the debugging information that contains sensitive data is pushed into the $response['secret'] array.
  The secret information array is unset (removed) prior to transmission. Therefore, the information is able
  to be transmitted for debugging purposes or removed to secure the transmissions. You can easily run the
  unit tests to see what information is transmitted in the $request and $response.


STABILITY
  It's assumed that most of the error checking is done by the server that sends shrine signals. 
  Therefore, only the minimal error checking is done within shrine. We keep the amounf of code
  in shrine to a minimum so that when brokers change their API designs, shrine can be updated
  very quickly.



+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

REQUESTS AND RESPONSES

  REQUEST: "get broker status"
  The 'get broker status' request grabs all of the information required to audit the brokerage account.

  $request = array(
    'request' => 'get broker status',
    'alias' => 'testar',
    'broker' => 'etrade',
    'spirit_time' => '2013-10-01 10:10:00',
    'id' => 'DFJF8I0' );

  RESPONSE:
   $response = array(
     'alias' => 'testar',
     'spirit_time' => '2013-10-01 10:10:00',
     'id' => 'DFJF8I0',
     'broker status' => array(
     'open_market' => 'yes',
     'cash_broker' => '3003',
     'margin' = 'yes',
     'broker_positions' => array(
       array( 'symbol' => 'ASTC', 'shares' => 101 ),
       array( 'symbol' => 'RHAT', 'shares' => 201 ),
       array( 'symbol' => 'FMCC', 'shares' => 301 ) ),
     'done_orders' => array(
       array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => 50, 'price' => 1.02,
       'date' => '2010-12-06 01:49:00', 'orderid' => 'AKFJD9393'),
       array( 'symbol' => 'RHAT', 't_type' => 'buy', 'shares' => 201, 'price' => 2.02,
       'date' => '2010-12-06 01:49:00', 'orderid' => 'BKFJD9393' ),
       array( 'symbol' => 'FMCC', 't_type' => 'buy', 'shares' => 301, 'price' => 3.02,
       'date' => '2010-12-06 01:49:00', 'orderid' => 'CKFJD9393' ) ),
     'open_orders' => array('symbol' => 'ASTC', 'shares' => 51 ) ) );



  REQUEST: "get quotes"
    This request should returns anything related to symbols, options, etc.
    For this request the request id, and spirit_time are not used, however
    they are still included. This request includes a 'get quotes' array to
    specify the specific symbols that the upstream server wants data about.
    The fundamentals and all other details related to the symbol should be
    returned in the response at once.

     $request = array(
       'request' => 'get quotes',
       'alias' => 'quotes',
       'get quotes' = array('ASTC', 'FMCC', 'FNMA'),
       'spirit_time' => '2013-10-01 10:10:00',
       'id' => 'ADFJF8' );

     $response = array( 
       'alias' => 'testar',
       'spirit_time' => '2013-10-01 10:10:00',
       'id' => 'ADFJF8',
       'quotes' => array(
         'GOOG' => '24.63',
         'AAPL' => '15.15',
         'MSFT' => '1.81');


  REQUEST: "order"
    This request includes all the information needed to place an order including the limit price,
    symbol, and side of the order. The upstream server must provide the request id, alias, and a
    array that has all of the orders in it. It is important that there only be ONE instance of a
    symbol in the order array. The tracking id may be set to symbol.id.request[id].

    $request = array(
      'request' => 'order',
      'alias' => 'testar',
      'spirit_time' => '2013-08-01 03:09:00',
       'id' => 'ZDFJF8I',
       'order' => array (
         array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => '1278', 'price' => '0.77' ),
         array( 'symbol' => 'GLYE', 't_type' => 'buy', 'shares' => '920', 'price' => '1.07' ),
         array( 'symbol' => 'KOOL' 't_type' => 'buy', 'shares' => '911', 'price' => '1.08' ) ) );

     RESPONSE:
     $this->response = array(
       'alias' => 'testar',
       'spirit_time' => '2013-08-01 03:09:00',
       'id' => 'ZDFJF8I',
       'successful broker orders' => array(
         array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => 101 ),
         array( 'symbol' => 'RHAT', 't_type' => 'buy', 'shares' => 201 ),
         array( 'symbol' => 'FMCC', 't_type' => 'buy', 'shares' => 301 ) ) );


+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

SETUP
  . Create an account at http://openshift.com
  . Click "Create My First App"
  . Click PHP 5.4
  . For the source code, use https://bitbucket.org/PagerProject/shrine & master
  . Set the openshift domain to shrine<shrine number> only.
  . Set the application name to "etrade"
  . Choose "No Scaling"
  . Click "Create Application" (and it will take some time to process)
  . Click "Continue"
  . Then, you will have a namespace (like http://etrade-shrine.rhcloud.com)
  . Add a "cartridge" by clicking "MySQL 5.5"
  . Save the database credentials
  . Add a "cartridge" by clicking "PHPMyAdmin"


  SSH SETUP
  . Install ssh (if you have not already)
  . $ ssh-keygen -t rsa -C "your@email.com"
  . When prompted, name the file "id_rsa"
  . When prompted, add a passphrase
  . eval "$(ssh-agent -s)"
  . $ ssh-add id_rsa
  . When prompted, enter the passphrase
  . Copy the contents of the id_rsa.pub file
  . Click the "etrade" application name in openshift
  . Click the "Add an SSH public key to your account" link
  . Paste the text into the form requesting the public key on openshift
  . Add any key name that you like
  . Click "Create" to store the information in the form


  GIT SETUP
  . Install GIT (if you have not already)
  . Click the "Applications" tab
  . Click "etrade"
  . Copy the text under "Source Code" on the right (ssh://....)
  . Go to your localhost webroot server space
  . git clone https://bitbucket.org/PagerProject/shrine.git (to clone the code locally)
  . git remote add openshift ssh://[some nunber]@my-shrine[shrine number].rhcloud.com/~/git/etrade.git/
  . git remote -v (This should show that you have 2 remote GIT repositories
  . git push openshift master (to push the code to openshift)
  . SEE: http://stackoverflow.com/questions/12657168/can-i-use-my-existing-git-repo-with-openshift 

  SHRINE SETUP
  . Rename the top level "etrade" folder to "shrine"
  . Edit the shrine/config.php
  . Set the $server variable to "programmer"
  . Log into PHPMyAdmin to find the server IP (and port)
  . Remove the port from the IP address (127.X.X.X NOT 127.X.X.X:3306)
  . Set DB_HOST to the IP address
  . Set DB_USER to the openshift MySQL username
  . Set DB_PASS to the openshift MySQL password
  . Set PROPHET_URL to 'prophet-signs.rhcloud.com/relay.php'
  . Set SHRINE_USER to 'shrine<your shrine number>'
  . Set SHRINE_PASS to the assigned password
  . Set SHRINE_URL to the shrine address (ex: 'etrade-shrine<number>.rhcloud.com')
  . Save the config.php

  SHRINE UPDATE
  . $ cd localhost/shrine/
  . $ git add ./*
  . $ git commit -m 'My First shrine Update'
  . $ git push origin master
  . Enter the password when prompted


// END
