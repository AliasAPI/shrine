<?php


function round_decimal_down( $number, $precision )
{
  /*
   * CONCEPT:
   * round_decimal_down() (rounds down) to the nearest decimal place.
   * Test:
   * $answer = round_decimal_down( 1.340000, 2 );
   * echo $answer;
   *
   * CAUTION:
   */


  $coefficient = pow( 10, $precision );

  $number = $number * $coefficient;

  // WARNING: The following line is required due to the float issues.
  $number = strval( $number );

  return floor( "{$number}" ) / $coefficient;
}


?>
