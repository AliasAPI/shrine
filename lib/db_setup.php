<?php

// $request = array('request' => 'get broker status', 'alias' => 'testar', 'broker' => 'testbroker', 'id' => 'TRACKIT' );
// $request = array('request' => 'get broker status', 'alias' => 'testar', 'broker' => 'etrade', 'id' => 'TRACKIT' );
// $request = array('request' => 'get broker status', 'alias' => 'testar', 'broker' => 'tradier', 'id' => 'TRACKIT' );
// $db_setup = new db_setup();
// $db_setup->build( $request );


class db_setup
{
  private $account_setup;
  private $accounts_table_created;
  private $create_accounts_table;

  public $db;
  private $just_created;

  public $debug = array();
  private $request = array();

  public $warning = array();
  public $emergency;


  public function build( $request )
  {
    $this->request = array();
    $this->request = $request;

    if( !defined( DB_NAME ) )
    {
      define('DB_NAME', $this->request['broker'] );
    }

    $this->__include();

    $this->connect();

    $this->create_database();

    $this->select();

    $this->create_tables();

    if( $this->check_alias_config() )
    {
      $this->create_account();
    }

    // var_dump( $this->debug ); die();
    return array( $this->db, $this->debug );
  }



  private function __include()
  {
    // Initialize vars
    $this->account_setup = array();

    include( dirname(__FILE__) . "/../config.php" );
    require_once( dirname(__FILE__) . "/adodb5/adodb.inc.php" );

    // echo "here";
    // var_dump( $create_accounts_table );
    // die();

    $this->account_setup = $account_setup;
    $this->create_accounts_table = $create_accounts_table;

    $this->debug[] = "The database has been set to [ " . DB_NAME . " ]";
  }



  private function check_alias_config()
  {
    // If the alias is not set in the account_setup array located in the config.php
    if( !isset( $this->account_setup[$this->request['alias']] ) )
    {
      $this->emergency = "The [ " . $this->request['alias'] . " ] alias has NOT been added to the /shrine/config.php";
    }
    else
    {
      $this->debug[] = "The [ " . $this->request['alias'] . " ] alias is in the config.php";

      return TRUE;
    }
  }



  private function connect()
  {
    // Instantiate the class
    $this->db = NewADOConnection('mysql');

    // Try to connect . . .
    $this->db->Connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

    // IF the database is NOT connected
    if ( $this->db->_errorMsg )
    {
      $this->warning[] = DB_NAME . " database NOT connected yet.";
    }
    else
    {
      $this->debug[] = DB_NAME . " database connected.";
    }
  }



  private function create_account()
  {
    // Try to find the alias in the broker's `accounts` table
    $result = $this->db->Execute("SELECT `alias` FROM `accounts`
      WHERE `alias` = '" . $this->request['alias'] . "' ");

    if( isset( $this->request['alias'] ) && !empty($this->request['alias'] ) )
    {
      if( $result->fields[0] !== $this->request['alias'] )
      {
        // If the shrine administrator set up an query for the alias
        if( isset( $this->account_setup[$this->request['alias']] ) )
        {
          $result = $this->db->Execute("INSERT IGNORE INTO `accounts` (`alias`)
            VALUES ('" . $this->request['alias'] . "') ");

          // var_dump( $results->fields );
          //  var_dump( $this->account_setup );

          // Try to find the alias in the broker's `accounts` table
          $result = $this->db->Execute("SELECT `alias` FROM `accounts`
            WHERE `alias` = '" . $this->request['alias'] . "' ");

          // If the alias was successfully added to the accounts table
          if( $result->fields[0] === $this->request['alias'] )
          {
            $this->debug[] = "The [ " . $this->request['alias'] . " ] alias was added to the `accounts` table.";

            // If the account_setup is set for the alias in shrine/config.php, add the account attributes
            if( $this->account_setup )
            {
              // Insert the remaining account attributes if the account is new
              $result = $this->db->Execute( $this->account_setup[$this->request['alias']] );
            }
          }
        }
        else
        {
          $this->warning[] = "The [ " . $this->request['alias'] . " ] alias has NOT been added to the config.php";
        }
      }
    }
  }



  public function create_database()
  {
    // $db->Connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );
    $result = $this->db->Execute("SHOW DATABASES WHERE `Database`
      LIKE '" . DB_NAME . "' ");

    // IF the database does NOT exist . . .
    if( $result->fields[0] !== DB_NAME )
    {
      mysql_query("CREATE DATABASE IF NOT EXISTS `" . DB_NAME . "` ");

      $this->db->Connect( DB_HOST, DB_USER, DB_PASS, DB_NAME );

      $result = $this->db->Execute("SHOW DATABASES WHERE `Database`
        LIKE '" . DB_NAME . "' ");

      // If the database does NOT exist . . .
      if( $result->fields[0] !== DB_NAME )
      {
        $this->response['warning'][] = DB_NAME . " database creation failed.";
      }
      else
      {
        $this->response['debug'][] = DB_NAME . " database newly created!";
        $this->just_created = TRUE;
      }
    }
  }



  private function create_tables()
  {
    // If the database was just created
    if( $this->just_created )
    {
      $this->db->Execute( $this->create_accounts_table );

      $result = $this->db->Execute("SHOW TABLES LIKE '%accounts%' ");

      // If the accounts table was NOT created successfully . . .
      if( $result->fields[0] !== 'accounts' )
      {
        $this->warning[] = "Failed to create the `accounts` table! [ " . $this->create_accounts_table . " ]";
      }
      else
      {
        $this->debug[] = "The `accounts` table was successfully created.";
        $this->accounts_table_created = TRUE;
      }
    }
  }



  private function select()
  {
    $database_selected = $this->db->SelectDB( DB_NAME );

    if( $database_selected == TRUE )
    {
      $this->debug[] = DB_NAME . " selected. ";
    }
    else
    {
      $this->warning[] = DB_NAME . " was NOT selected. ";
    }
  }


}


?>
