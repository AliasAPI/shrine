<?php


function generate_id()
{
  /*
   * CONCEPT:
   * generate_id() creates a random key for each subtransaction.
   * It's designed to mimic the tracking number that must be received
   * from the broker.
   *
   * CAUTION:
   */


  // Start with a blank orderid
  $id = '';

  // Generate a random 5 character A-Z0-9 string
  for( $i = 0; $i < 5; $i++ )
  {
    $d = rand( 1,30 )%2;

    $id .= $d ? chr(rand(65,90)) : chr(rand(48,57));
  }

  return $id;
}


?>


