<?php


function get_backtrace_detail()
{
  // Initialize vars
  $output = array();

  // Get the bactrace . . .
  $traces = debug_backtrace();

  // Reverse the backtrace to make it easy to read
  $traces = array_reverse( $traces );

  foreach( $traces AS $element => $step )
  {
    if( $step['function'] !== 'get_backtrace_detail'
      && $step['function'] !== 'emergency' )
    {
      $directory = basename( dirname( $step['file'] ) );
      $file = basename( $step['file'] );
      $line = $step['line'];

      if( isset( $step['class'] ) )
      {
        $function = $step['class'] . "->" . $step['function'];
      }
      else
      {
        $function = $step['function'];
      }

      $output[$element]['path'] = "$directory/$file ( Line: $line ) $function()";

      if( isset( $step['args'] ) && !empty( $step['args'] ) )
      {
        $output[$element]['input'] = $step['args'];
      }
    }
  }

  // echo "<pre>"; print_r( $output ); echo "</pre>"; die();

  return $output;
}


?>
