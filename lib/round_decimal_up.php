<?php


function round_decimal_up( $number, $precision )
{
  /*
   * CONCEPT:
   * round_decimal_up() (rounds up) to the nearest decimal place.
   * Test:
   * $answer = round_decimal_up( 1.340000, 2 );
   * echo $answer;
   *
   * CAUTION:
   */


  $coefficient = pow( 10, $precision );

  $coefficient = strval( $coefficient );

  $number = $number * $coefficient;

  // WARNING: The following line is required:
  $number = strval( $number );

  // Using the braces here redundantly makes $number a string (rather than float).
  //  return ceil( "{$number}" ) / $coefficient;
  return ceil( "{$number}" ) / $coefficient;
}



