<?php


function get_filename_extension( $filename )
{
  $pos = strrpos( $filename, '.' );

  if( $pos === FALSE )
  {
    return FALSE;
  }
  else
  {
    return substr( $filename, $pos + 1 );
  }
}


?>
