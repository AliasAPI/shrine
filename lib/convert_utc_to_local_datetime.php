<?php


// convert_utc_to_local_datetime( '2014-06-23 10:20:00', '2014-06-23 06:20:00' );

function convert_utc_to_local_datetime( $utc_datetime, $datetime_input )
{
  // print "UTC DATETIME: " . $utc_datetime . "<br>\n";
  // print "DATETIME INPUT: " . $datetime_input . "<br>\n";

  // Calculate the offset in seconds
  $offset = date('Z', strtotime( $utc_datetime ) );

  // print "OFFSET IN SECONDS: $offset <br>\n";

  // Finally, add the offset to the integer timestamp of your original datetime:
  $converted_datetime = date('Y-m-d H:i:s', strtotime( $datetime_input ) + $offset );

  // print "CONVERTED DATETIME: " . $converted_datetime . "<br>\n";

  return $converted_datetime;
}


?>
