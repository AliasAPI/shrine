<?php


function backtrace( $level = NULL )
{

  if( is_numeric( $level ) )
  {
    // Run the backtrace . . .
    $ref = debug_backtrace();

    // Initialize var
    $sp = 0;
    $trace = NULL;
    $referrer = NULL;

    // $file   = str_replace('.php', '', basename($ref[1]['file']));
    if( isset( $ref[$level]['class'] ) && !empty( $ref[$level]['class'] ) )
    {
      $class = $ref[$level]['class'] . "->";
    }
    else
    {
      $class = '';
    }

    $function = $ref[$level]['function'];
    // $line   = $ref[$level]['line'];

    return "$class$function() ";
  }
  else
  {
    echo "You need to be about setting the \$level for debugs.php->referrer<br>";
  }
}


?>
