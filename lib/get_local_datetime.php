<?php



function get_local_datetime()
{
  // Set default timezone to America/New_York for date function calls
  date_default_timezone_set('America/New_York');

  $local_datetime = date('Y-m-d H:i:s', strtotime("now") );

  // echo "LOCAL TIME: " . $local_time . "<br>\n";

  return $local_datetime;
}


?>
