<?php


function calculate_profit( $final_value, $beginning_value )
{
  $profit = $final_value / $beginning_value;

  comment("[ \$$final_value ] final_value / [ \$$beginning_value ] beginning_value =  [ $profit% ] profit ");

  $profit = round_decimal_down( $profit, 2 );

  comment("profit = [ $profit% ] ");

  $profit = $profit * 100;

  comment("profit = [ $profit% ] ");

  return $profit;
}


?>
