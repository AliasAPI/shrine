<?php


function get_utc_datetime()
{
  // Set the local time to UTC
  date_default_timezone_set('UTC');

  $utc_datetime = date('Y-m-d H:i:s', strtotime( gmdate("M d Y h:i:s A") ) );

  // echo "UTC: " . $utc . "<br>\n";

  return $utc_datetime;
}


?>
