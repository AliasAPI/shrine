<?php


function bullet_point_array( $array )
{
  $output = '<ul style="list-style-type:square">';

  if( is_array( $array ) )
  {
    foreach( $array AS $key => $value )
    {
      if( is_array( $value ) )
      {
        $output .= '<li>' . $key . ' ' . bullet_point_array( $value ) . '</li>';
      }
      else
      {
        $output .= '<li>' . $key . ' => ' . $value . '</li>';
      }
    }
  }

  $output .= '</ul>';

  return $output;
}


?>
