<?php


class json
{
  private $debug;


  public function encode( $array )
  {
    // Encode the request in JSON and UTF8
    $json = json_encode( $array );
    $json = utf8_encode( $json );

    return $json;
  }



  public function to_array( $json )
  {
    /*
     * CONCEPT:
     *
     * CAUTION:
     */

    // Format the posted JSON
    $json = trim( $json );
    $json = utf8_encode( $json );
    $json = str_replace( "\n", '', $json );

    // Add the posted JSON to the debug response
    // $this->debug[] = "Posted JSON: [ " . $post . " ] ";

    // Convert the posted JSON into an array
    $json = (array)json_decode( $json, TRUE );

    if( !$json )
    {
      switch( json_last_error() )
      {
      case JSON_ERROR_NONE:
        $this->debug[] = 'Error: No JSON';
        break;
      case JSON_ERROR_DEPTH:
        $this->debug[] = 'Maximum JSON stack depth exceeded';
        break;
      case JSON_ERROR_STATE_MISMATCH:
        $this->debug[] = 'JSON Underflow or the modes mismatch';
        break;
      case JSON_ERROR_SYNTAX:
        $this->debug[] = 'JSON Syntax error, malformed JSON';
        break;
      case JSON_ERROR_CTRL_CHAR:
        $this->debug[] = 'Unexpected JSON control character found';
        break;
      case JSON_ERROR_UTF8:
        $this->debug[] = 'Malformed UTF-8 characters, JSON possibly incorrectly encoded';
        break;
      default:
        $this->debug[] = 'Unknown JSON error';
        break;
      }
    }

    return $json;
  }



  function format( $json )
  {
    $tab = "  ";
    $new_json = "";
    $indent_level = 0;
    $in_string = false;


    $json = trim( $json );
    $json = utf8_encode( $json );
    $json_obj = json_decode($json);

    if( $json_obj === FALSE )
    {
      return FALSE;
    }

    $json = json_encode( $json_obj );

    $len = strlen( $json );

    for( $c = 0; $c < $len; $c++ )
    {
      $char = $json[$c];
      switch($char)
      {
      case '{':
      case '[':
        if( !$in_string )
        {
          $new_json .= $char . "\n" . str_repeat($tab, $indent_level+1);
          $indent_level++;
        }
        else
        {
          $new_json .= $char;
        }
        break;
      case '}':
      case ']':
        if(!$in_string)
        {
          $indent_level--;
          $new_json .= "\n" . str_repeat($tab, $indent_level) . $char;
        }
        else
        {
          $new_json .= $char;
        }
        break;
      case ',':
        if(!$in_string)
        {
          $new_json .= ",\n" . str_repeat( $tab, $indent_level );
        }
        else
        {
          $new_json .= $char;
        }
        break;
      case ':':
        if(!$in_string)
        {
          $new_json .= ": ";
        }
        else
        {
          $new_json .= $char;
        }
        break;
      case '"':
        if($c > 0 && $json[$c-1] != '\\')
        {
          $in_string = !$in_string;
        }
      default:
        $new_json .= $char;
        break;
      }
    }

    return $new_json;
  }



  public function show( $json )
  {
    // Format the json with carriage returns
    $new_json = $this->format( $json );

    // Strip out the ugly characters
    $new_json = str_replace( '{', '', $new_json );
    $new_json = str_replace( '},', '', $new_json );
    $new_json = str_replace( "}", '', $new_json );
    $new_json = str_replace( "\"", '', $new_json );
    $new_json = str_replace( ",", '', $new_json );

    return $new_json;
  }


}


?>
