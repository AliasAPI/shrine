<?php


class test
{
  private $request = array();
  private $response = array();
  private $settings = array();

  private $db;
  private $account_setup;

  private $shrine_relay;
  private $prophet_user;
  private $prophet_pass;
  private $setup_sql;


  public function __construct( $request, $response, $settings )
  {
    // Include the files
    $this->__include();

    // Instantiate the class that has the CURL library wrapper
    $curl = new curl();

    // Set the private variables for this class
    $this->set_settings( $request, $response, $settings );

    $this->setup_test();

    // Post a CURL request to the top level of the shrine directory ( targeted at /shrine/relay.php )
    list( $this->response, $debug ) =
      $curl->transmit( $this->shrine_relay, $this->prophet_user, $this->prophet_pass, $this->request );

    // $this->response['debug']['curl'] = $debug;

    echo "<pre>";
    echo "<table width=100%><tr><td bgcolor=lightblue><center><font size=+2><b>" . $this->settings['description'] . "</b></font></center></b></td></tr></table><br>";

    // echo "<hr><b>RESULT:</b><br>";
    $this->get_results();

    echo "<hr><b>REQUEST:</b><br>";
    print_r( $this->request );

    echo "<hr><b>RESPONSE:</b><br>";
    print_r( $this->response );

    echo "<hr><b>DEBUG:</b><br>";
    print_r( $debug );
  }


  private function __include()
  {
    // Include the main config file
    require_once( dirname(__FILE__) . "/../config.php" );

    $this->account_setup = array();
    $this->account_setup = $account_setup;

    // require_once( dirname(__FILE__) . "/../index.php");
    require_once( dirname(__FILE__) . "/db_setup.php");

    // Include the class that contains the CURL library
    require_once( dirname(__FILE__) . "/curl.php" );
  }



  private function get_results()
  {
    // emergency
    if( $this->response['emergency'] !== $this->expected_response['emergency'] )
    {
      // emergency( "[ $file ] TEST FAILED<br>raised = [ $raised ] exception = [ $exception ]<br><br>$emergencies ", 'continue' );
      echo "<table bordercolor=red border=5 width=100% NOWRAP><tr><td>TEST FAILED<br>
        \$response['emergency'] is [ " . $this->response['emergency'] . " ] and it should be [ " . $this->expected_response['emergency'] . " ]</td></tr></table> ";
    }
    // warning
    elseif( $this->expected_response['warning'][0] !== $this->response['warning'][0] )
    {
      echo "<table bordercolor=red border=5 width=100% NOWRAP><tr><td>TEST FAILED<br>
        \$response['warning'] is [ " . print_r( $this->response['warning'][0], TRUE ) . " ]
        and it should be [ " . print_r( $this->expected_response['warning'][0], TRUE ) . " ]</td></tr></table> ";
    }
    else
    {
      // comment( "[ $file ] TEST PASSED ", 9 );
      echo "<table bordercolor=green border=5 width=100% NOWRAP><tr><td NOWRAP><b>TEST PASSED</b></td></tr></table><br>";
    }
  }



  private function set_settings( $request, $response, $settings )
  {
    $this->prophet_user = ( $settings['prophet_user'] !== NULL ) ? $settings['prophet_user'] : PROPHET_USER;
    $this->prophet_pass = ( $settings['prophet_pass'] !== NULL ) ? $settings['prophet_pass'] : PROPHET_PASS;
    $this->shrine_relay = SHRINE_URL . '/relay.php';

    $this->request = $request;
    $this->expected_response = $response;
    $this->settings = $settings;
  }



  private function setup_test()
  {
    // Connect to the database and setup a database for the broker
    $db_setup = new db_setup();

    list( $this->db, $debug ) = $db_setup->build( $this->request );

    // var_dump( $this->debug);
    // var_dump( $this->db );

    // Delete the test account. It will be replaced automatically.
    $this->db->Execute("DELETE FROM `accounts` WHERE `alias` = 'testar' ");

    list( $this->db, $debug ) = $db_setup->build( $this->request );

    // If setup_sql
    $this->db->Execute( $this->settings['setup_sql'] );
  }


}


?>
