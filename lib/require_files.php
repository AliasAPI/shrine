<?php


function require_files( $file_array )
{
  foreach( $file_array AS $file => $index )
  {
    // If the file has an extention (and is not a directory)
    if( strpos($file, '.' ) )
    {
      // echo "file: $file<br>";
      require_once( $file );
    }
  }
}


?>
