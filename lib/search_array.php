<?php


function search_array( $item, $multi_array, $partial_matches = FALSE, $search_keys = FALSE )
{
  /*
   * CONCEPT:
   * search_array() recursively searches through an array for the $item specified.
   *
   * CAUTION:
   * Make sure to set $search_keys = TRUE. Consider setting $search_keys = FALSE if
   * searching for numbers.
   */

  if( !is_array( $multi_array ) )
  {
    comment( "Send an array to me, would ya?", 1 );
    return FALSE;
  }

  // Loop through the multideminsional array
  foreach( $multi_array AS $key => $value )
  {
    $what = ( $search_keys ) ? $key : $value;

    // If the $item is found
    if( $item === $what )
    {
      comment( "\$item [ $item ] found! ", 1 );
      return $item;
    }
    // If it is OK to find a partial match
    elseif( $partial_matches && @strpos( $what, $item ) !== FALSE )
    {
      comment( "partial match \$item [ $item ] found! ", 1 );
      return $item;
    }
    // If the value is an array and the $item is found
    elseif( is_array( $value ) && search_array( $item, $value, $partial_matches, $search_keys ) !== FALSE )
    {
      comment( "\$value \$item [ $item ] found! ", 1 );
      return $item;
    }
  }

  // $notfound = 'failz';

  return FALSE;
}


?>
