<?php


/*


// Set this to 'yes' if you want the secrets to be displayed and transmitted
define('SHOW_SECRETS', 'no' );

// Set the expected username and pass for the server that sends requests to shrine
define('PROPHET_USER', '*' );
define('PROPHET_PASS', '*' );


// Automatically set the MySQL credentials based on the environment
if( dirname(__FILE__) === '*' )
{
  define('DB_HOST', '*');
  define('DB_USER', '*');
  define('DB_PASS', '*');

  // define('DB_NAME', 'en'); // This will be set in the broker folders

  define('PROPHET_URL', '*');
  define('SHRINE_URL', '*');
}
else
{
  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', '*');
  // define('DB_NAME', 'en'); // This will be set in the broker folders

  define('PROPHET_URL', 'localhost/prophet/relay.php');
  define('SHRINE_URL', 'localhost/shrine');
}



// ETRADE
define('SHRINE_SDK_ENV', 'prod');

if( DB_NAME ===  'etrade' )
{
  $create_accounts_table = "CREATE TABLE IF NOT EXISTS `accounts`
    (`id` int(11) NOT NULL AUTO_INCREMENT,
      `alias` varchar(500) NOT NULL,
      `account_id` varchar(500) NOT NULL,
      `oauth_consumer_key` varchar(500) NOT NULL,
      `oauth_consumer_secret` varchar(500) NOT NULL,
      `oauth_token` varchar(500) DEFAULT NULL,
      `oauth_token_secret` varchar(500) DEFAULT NULL,
      `oauth_verifier` varchar(500) DEFAULT NULL,
      `access_token` varchar(500) DEFAULT NULL,
      `access_token_secret` varchar(500) DEFAULT NULL,
      `timestamp` datetime DEFAULT NULL,
      `received` text NULL,
      `response` text NULL,
      PRIMARY KEY (`id`),
    UNIQUE KEY `alias` (`alias`) )
    ENGINE=InnoDB  DEFAULT CHARSET=latin1 ";

  define('TEST_OAUTH_CONSUMER_KEY', '*' );
  define('TEST_OAUTH_CONSUMER_SECRET', '*');

  define('PROD_OAUTH_CONSUMER_KEY', '*' );
  define('PROD_OAUTH_CONSUMER_SECRET', '*');

  $account_setup['testar'] = "UPDATE `accounts` SET `account_id` = '69489191',
    `oauth_consumer_key` = '" . PROD_OAUTH_CONSUMER_KEY . "',
    `oauth_consumer_secret` = '" . PROD_OAUTH_CONSUMER_SECRET . "'
    WHERE `alias` = 'testar' ";
}


if( DB_NAME === 'testbroker' )
{
  $create_accounts_table = "CREATE TABLE IF NOT EXISTS `accounts`
    (`alias` varchar(100) NOT NULL,
      `received` text NULL,
      `response` text NOT NULL,
      UNIQUE KEY `alias` (`alias`) )
      ENGINE=InnoDB
      DEFAULT CHARSET=latin1 ";

  $account_setup['testar'] = "UPDATE `accounts` SET `response` = ''
    WHERE `alias` = 'testar' ";

  $account_setup['testar2'] = "UPDATE `accounts` SET `response` = ''
    WHERE `alias` = 'testar' ";
}



if( DB_NAME === 'tradier' )
{
  $create_accounts_table = "CREATE TABLE IF NOT EXISTS `accounts`
    (`id` int(11) NOT NULL AUTO_INCREMENT,
      `alias` varchar(500) NOT NULL,
      `access_token` varchar(500) NOT NULL,
      `account_id` varchar(500) NOT NULL,
      `received` text NULL,
      `response` text NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `alias` (`alias`) )
      ENGINE=InnoDB  DEFAULT CHARSET=latin1 ";

  $account_setup['testar'] = "UPDATE `accounts` SET
    `access_token` = '*',
    `account_id` = '*'
    WHERE `alias` = 'testar' ";

  // var_dump( $account_setup );
}


// sets default timezone to America/New_York for date function calls
date_default_timezone_set('America/New_York');


 */


?>
