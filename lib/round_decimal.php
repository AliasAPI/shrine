<?php


function round_decimal( $number, $precision )
{
  /*
   * CONCEPT:
   * round_decimal() rounds to the nearest decimal place.
   * Test:
   * $answer = round_decimal( 1.340000, 2 );
   * echo $answer;
   *
   * CAUTION:
   */

  $coefficient = pow( 10, $precision );

  // $coefficient = strval( $coefficient );

  return round( $number * $coefficient ) / $coefficient;
}


?>
