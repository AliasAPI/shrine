<?php


function include_lib()
{
  // echo $library;

  if( defined('CODE_PATH') )
  {
    $library = CODE_PATH . 'lib/';

    // echo "$library <hr>";

    require_once( "$library" . "put_files_in_array.php");
    require_once("$library" . "require_files.php");


    $files = put_files_in_array( $library );

    // echo "<pre>files " . print_r( $files, TRUE ) . "</pre><hr> ";

    require_files( $files );
  }
  else
  {
    die('CODE_PATH is not defined');
  }
}


?>
