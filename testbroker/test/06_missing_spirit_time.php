<?php


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'get broker status';
$request['alias'] = 'testar';
$request['broker'] = 'testbroker';
$request['spirit_time'] = '';
$request['id'] = time();

// Define the expected shrine response
$response['emergency'] = 'The spirit_time is [  ]';

// Define the extra settings
$settings['description'] = 'Prophet sends a request without a timestamp.';
$settings['prophet_user'] = NULL;
$settings['prophet_pass'] = NULL;

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
