<?php


/*
// RESULTS
$emergencies .= ( !$response['broker status'] )
  ? "\$response['broker status'] not returned. <br>" : '';

$emergencies .= ( !$response['broker status']['open_market'] )
  ? "\$response['broker status']['open_market'] not returned. <br>" : '';
$emergencies .= ( !$response['broker status']['cash_broker'] )
  ? "\$response['broker status']['cash_broker'] not returned. <br>" : '';
$emergencies .= ( !$response['broker status']['margin'] )
  ? "\$response['broker status']['margin'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['broker_positions'] )
  && $response['broker status']['broker_positions'] !== NULL )
  ? "\$response['broker status']['broker_positions'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['done_orders'] )
  && $response['broker status']['done_orders'] !== NULL )
  ? "\$response['broker status']['done_orders'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['open_orders'] )
  && $response['broker status']['open_orders'] !== NULL )
  ? "\$response['broker status']['open_orders'] not returned. <br>" : '';
 */




$datetime = date( 'Y-m-d H:i:s', time() );


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'get broker status';
$request['alias'] = 'testar';
$request['broker'] = 'testbroker';
$request['demon'] = array(
  'broker status' => array(
    'open_market' => 'yes',
    'cash_broker' => '3003',
    'margin' => 'yes',
    'broker_positions' => array(
      array( 'symbol' => 'AAA', 'shares' => 101 ),
      array( 'symbol' => 'BBB', 'shares' => 201 ),
      array( 'symbol' => 'CCC', 'shares' => 301 ) ),
    'done_orders' => array(
      array( 'symbol' => 'AAA', 't_type' => 'buy', 'shares' => 50, 'price' => 1.02,
      'date' => "" . $datetime . "", 'orderid' => 'AKFJD9393'),
      array( 'symbol' => 'BBB', 't_type' => 'buy', 'shares' => 201, 'price' => 2.02,
      'date' => "" . $datetime . "", 'orderid' => 'BKFJD9393' ),
      array( 'symbol' => 'CCC', 't_type' => 'buy', 'shares' => 301, 'price' => 3.02,
      'date' => "" . $datetime . "", 'orderid' => 'CKFJD9393' ) ),
    'open_orders' => array('symbol' => 'AAA', 'shares' => 51 ) ) );
$request['spirit_time'] = $datetime;
$request['id'] = time();

// Define the expected shrine response

// Define the extra settings
$settings['description'] = 'get_broker_status successfully returns logged_in, cash_broker, open_market.';

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
