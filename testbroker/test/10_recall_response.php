<?php


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");


// Define the prophet request
$request['request'] = 'order';
$request['alias'] = 'testar';
$request['broker'] = 'testbroker';
$request['order'] = array (
  array( 'symbol' => 'AAA', 't_type' => 'buy', 'shares' => '100', 'price' => '1.01' ),
  array( 'symbol' => 'BBB', 't_type' => 'buy', 'shares' => '200', 'price' => '2.02' ),
  array( 'symbol' => 'CCC', 't_type' => 'buy', 'shares' => '300', 'price' => '3.03' ) );
$request['spirit_time'] = '2014-12-18 03:03:03';
$request['id'] = 'RECALL';

// Define the expected shrine response
$response['warning'][] = 'Duplicate request received. Returning the recorded response.';

// Define the extra settings
$settings['description'] = 'The previously recorded broker response is returned.';

$settings['setup_sql'] = "UPDATE `accounts` SET
  `received` = '{\"request\":\"order\",\"alias\":\"testar\",\"broker\":\"testbroker\",\"spirit_time\":\"2013-08-01 03:09:00\",\"id\":\"RECALL\",\"order\":[{\"symbol\":\"AAA\",\"t_type\":\"buy\",\"shares\":\"101\",\"price\":\"1.01\"},{\"symbol\":\"BBB\",\"t_type\":\"buy\",\"shares\":\"202\",\"price\":\"2.02\"},{\"symbol\":\"CCC\",\"t_type\":\"buy\",\"shares\":\"303\",\"price\":\"3.03\"}]}',
  `response` = '{\"alias\":\"testar\",\"spirit_time\":\"2013-08-01 03:09:00\",\"id\":\"RECALL\",\"successful broker orders\":[{\"symbol\":\"AAA\",\"t_type\":\"buy\",\"shares\":101},{\"symbol\":\"BBB\",\"t_type\":\"buy\",\"shares\":202},{\"symbol\":\"CCC\",\"t_type\":\"buy\",\"shares\":303}]}'
  WHERE `alias` = '" . $request['alias'] . "' ";

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
