<?php

/*
 * CONCEPT:
 * Get some Quotes
 * 1. Symbols are provided in an array.
 * 2. Send a request to the API with the symbol array and get back quotes.
 * 3. Format the quotes with the translator() class
 * 4. Return the quotes
 *
 * CAUTION:
 * 2DO: What about missing quotes (or symbols that have been renamed)?
 *
 * $this->response = array(
 * 'alias' => 'testar',
 * 'spirit_time' => '2013-08-01 03:09:00',
 * 'id' => 'DFJF8I0',
 * 'quotes' => array(
 * 'ASTC' => '0.69',
 * 'FMCC' => '1.43',
 * 'FNMA' => '1.54' ) );
 */



// RESULTS
// Do not return FMCC or FNMA because they are OTC symbols (that Tradier cannot buy or sell)
/*
$emergencies .= ( !$response['quotes']['GOOG'] )
  ? "\$quotes['GOOG'] not returned. <br>" : '';

$emergencies .= ( !$response['quotes']['AAPL'] )
  ? "\$quotes['AAPL'] not returned. <br>" : '';
*/


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'get quotes';
$request['alias'] = 'testar';
$request['broker'] = 'testbroker';
$request['get quotes'] = array(
  'AAA' => NULL,
  'BBB' => NULL,
  'CCC' => NULL );
$request['demon']['quotes'] = array( 'AAA' => 1.01, 'BBB'=> 2.02, 'CCC' => 3.03 );
$request['spirit_time'] = "2013-06-26 03:06:27";
$request['id'] = time();


// Define the expected shrine response
// $response[''] = '';

// Define the extra settings
$settings['description'] = 'Get the quotes successfully from the broker.';

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
