<?php


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'get broker status';
$request['alias'] = 'testar';
$request['broker'] = 'badbroker';
$request['spirit_time'] = '2014-12-09 23:23:23';
$request['id'] = time();

// Define the expected shrine response
$response['emergency'] = 'This shrine does not handle the [ badbroker ] broker.';

// Define the extra settings
$settings['description'] = 'The broker is not valid for this shrine.';


// RUN THE TEST
$test = new test( $request, $response, $settings );


?>

