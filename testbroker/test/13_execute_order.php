<?php


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'order';
$request['alias'] = 'testar';
$request['broker'] = 'testbroker';
$request['order'] = array(
  array( 'symbol' => 'AAA', 't_type' => 'buy', 'shares' => '101', 'price' => '1.01' ),
  array( 'symbol' => 'BBB', 't_type' => 'buy', 'shares' => '202', 'price' => '2.02' ),
  array( 'symbol' => 'CCC', 't_type' => 'buy', 'shares' => '303', 'price' => '3.03' ) );
$request['spirit_time'] = date('Y-m-d H:i:s', time() );
$request['id'] = time();

// Define the extra settings
$settings['description'] = 'Buy shares of three different stocks.';

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
