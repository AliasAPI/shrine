<?php


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request = array();

  // Define the expected shrine response
$response['emergency'] = 'The request is missing.';

// Define the extra settings
$settings['description'] = 'Prophet sends an empty request.';
$settings['prophet_user'] = NULL;
$settings['prophet_pass'] = NULL;

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
