<?php


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'get broker status';
$request['alias'] = 'testar';
$request['broker'] = 'testbroker';
$request['spirit_time'] = '2014-06-15 14:10:00';
$request['id'] = time();

// Define the expected shrine response
$response['emergency'] = 'The prophet user is NOT authorized.';

// Define the extra settings
$settings['description'] = 'Prophet sends a bad user and is NOT authorized';
$settings['prophet_user'] = 'bad user';
$settings['prophet_pass'] = NULL;

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
