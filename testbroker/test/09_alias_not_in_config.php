<?php


// This error is generated in /shrine/lib/db_setup.php

// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'get broker status';
$request['alias'] = 'alias_that_is_not_in_the_config_file';
$request['broker'] = 'tradier';
$request['spirit_time'] = '2014-12-09 23:23:23';
$request['id'] = time();

// Define the expected shrine response
$response['emergency'] = 'The [ alias_that_is_not_in_the_config_file ] alias has NOT been added to the /shrine/config.php';

// Define the extra settings
$settings['description'] = 'The alias does NOT exist in the /shrine/config.php file.';


// RUN THE TEST
$test = new test( $request, $response, $settings );


?>

