<?php


class order
{
  private $request;
  private $response;


  public function place_order( $request )
  {
    /*
     * CONCEPT:
     * process_trades() executes orders in the brokerage account.
     *
     * CAUTION:
     */

    $this->request = array();
    $this->request = $request;
    $this->response = array();

    if( $this->request['order'] )
    {
      $this->response['successful broker orders'] = $this->request['order'];
    }
    else
    {
      $this->response['warning'] = 'No order sent';
    }

    return $this->response;
  }


}


?>

