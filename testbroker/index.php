<?php


class testbroker
{
  private $db;
  private $request = array();
  private $response = array();


  public function run_request( $request, $response, $db )
  {
    // Initialize vars
    $this->db = $db;
    $this->request = $request;
    $this->response = $response;

    $this->__include();

    $this->check_account();

    $this->route_request();

    // Return the raw information. The information will be validated in jesus.php
    return $this->response;
  }



  private function __include()
  {
    require_once(dirname(__FILE__) . '/../lib/json.php');
    require_once(dirname(__FILE__) . '/order.php');
    require_once(dirname(__FILE__) . '/status.php');
    require_once(dirname(__FILE__) . '/quotes.php');
  }



  private function check_account()
  {
    $this->response['debug'][] = "Check the account here.";
  }



  private function route_request()
  {
    if( $this->request['request'] === 'get broker status' )
    {
      $this->response['debug'][] = 'shrine->route_request() get broker status';

      // Instantiate the class that gets the account's broker status
      $status = new status();

      $response = $status->get_status( $this->request );
    }
    elseif( $this->request['request'] === 'get quotes' )
    {
      $this->response['debug'][] = 'shrine->route_request() get quotes';

      // Instantiate the class that handles the quotes
      $quotes = new quotes();

      $response = $quotes->get_quotes( $this->request );
    }
    elseif( $this->request['request'] === 'order' )
    {
      $this->response['debug'][] = 'shrine->route_request() order';
      // $this->response['debug'][] = $this->request;

      // Instantiate the class that handles the orders
      $order = new order();

      $response = $order->place_order( $this->request );
    }

    // Merge the previously built response with the new information returned from the broker
    $this->response = array_merge( $this->response, $response );
  }


}


?>
