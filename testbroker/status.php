<?php


class  status
{
  private $request = array();
  private $response = array();


  public function get_status( $request )
  {
    /*
     * CONCEPT:
     * shrine/status.php retrieves the current status information from the broker.
     * Some brokers (like just2trade) automatically send back all of the information
     * immediately. If that happens, the attributes are sent to other functions to be
     * split up and returned to jesus. Otherwise, the functions pull the information.
     * The entire class is meant to compile a 'response' array to be sent to shrine
     *
     * CAUTION:
     * Use minimal error checking and data validation. jesus should validate the data.
     */

    $this->request = array();
    $this->request = $request;

    // Check whether the account still has margin
    $this->get_margin();

    // Check to see if the stock markets are open
    $this->check_markets();

    // Get the current cash balance
    $this->get_cash_broker();

    // Get the broker_positions to get the number of shares owned for each symbol
    $this->get_broker_positions();

    // Get the new done_orders
    $this->get_done_orders();

    // Get the number of shares owed in the future for each symbol
    $this->get_open_orders();

    return $this->response;
  }



  private function get_margin()
  {
    /*
     * CONCEPT:
     * check_markets() checks to see if the markets are currently open.
     *
     * CAUTION:
     */

    $this->response['broker status']['margin'] = ( $this->request['demon']['broker status']['margin'] )
      ? $this->request['demon']['broker status']['margin'] : '';
  }



  private function check_markets()
  {
    /*
     * CONCEPT:
     * check_markets() checks to see if the markets are currently open.
     *
     * CAUTION:
     */

    $this->response['broker status']['open_market'] = ( $this->request['demon']['broker status']['open_market'] )
      ? $this->request['demon']['broker status']['open_market'] : 'no';
  }



  private function get_broker_positions()
  {
    /*
     * CONCEPT:
     * get_broker_positions() all of the symbols and the number of shares for each stock owned in the brokerage account.
     * The array format: $broker_positions[] = array("$symbol" => array('shares' => "$shares"));
     *
     * CAUTION:
     * It's important to count all of the symbols currently owned so that sacrifice() will work correctly.
     */


    // If a request['done_orders'] has been sent, send it back
    $this->response['broker status']['broker_positions'] = ( $this->request['demon']['broker status']['broker_positions'] )
      ? $this->request['demon']['broker status']['broker_positions'] : NULL;
  }



  private function get_cash_broker()
  {
    /*
     * CONCEPT:
     * get_cash_broker() returns $S->cash_local for use as $S->cash_broker.
     * Note: This is only used when using the various testing modes.
     *
     * CAUTION:
     * Make sure to match the account number in the returned array.
     */

    $this->response['broker status']['cash_broker'] = ( $this->request['demon']['broker status']['cash_broker'] === '0' )
      ? '3003' : $this->request['demon']['broker status']['cash_broker'];
  }



  private function get_done_orders()
  {
    /*
     * CONCEPT:
     * demon->get_done_orders() gets the due_shares for each symbol.  For example, if god
     * wants to buy 5000 shares of a symbol, it is common that smaller amounts
     * of shares are bought (at different prices) until the order is filled. get_done_orders()
     * gets the smaller transactions and they are recorded as subtransactions.
     * Return an array like this:
     * $done_orders[] = array('symbol' => "$symbol", 't_type' => "$t_type", 'shares' => "$shares",
     * 'price' => "$price", 'date' => "$t_date", 'orderid' => "$orderid" );
     *
     * CAUTION: The broker may have a temporary page that lists the recent subtransactions and disappears.
     * It is very important to get the information from the permanent page that shows the subtransactions.
     * All done orders must have a datetime stamp in this format: 'YYYY-MM-DD HH:MM:SS'
     *
     * Just2Trade sends "a summary of completed trades (both executed and cancelled for the current day"
     * "The trade summaries only show aggregated totals for positions bought and sold and prices. That is,
     * if an order was executed in more than one fill, only the total quantity executed and the average
     * price of all executions will be shown" . . . so . . .
     *
     * . The report is limited to whole transactions (in cumulative amounts until the total is returned)
     * . That means jesus will have to ignore the number of shares being too high
     * . It can cause confusion on orders that are not completely filled within the day
     *
     * Scottrade
     * . may have a temporary page listing the subtransactions that disappears after login
     * . may list all of the subtransactions on a page (for the day, week, or whole month)
     *
     * TD Ameritrade
     * . may have an API, but the commission is $10 per transaction
     */


    // If a request['done_orders'] has been sent, send it back
    $this->response['broker status']['done_orders'] = ( $this->request['demon']['broker status']['done_orders'] )
      ? $this->request['demon']['broker status']['done_orders'] : NULL;
  }



  private function get_open_orders()
  {
    /*
     * CONCEPT:
     * get_open_orders() gets the symbol and number of due_shares
     * for each incomplete transaction.  The array format:
     * $open_orders[] = array('symbol' => "$symbol", 'shares' => "$open_shares" ));
     *
     * CAUTION:
     */

    // If a request['open_orders'] has been sent, send it back
    $this->response['broker status']['open_orders'] = ( $this->request['demon']['broker status']['open_orders'] )
      ? $this->request['demon']['broker status']['open_orders'] : NULL;
  }



  private function logout()
  {
    /*
     * CONCEPT:
     * logout() logs out of the brokerage account.
     *
     * CAUTION:
     */

    $this->response['broker status']['broker status']['logout'] = 'yes';
  }


}

