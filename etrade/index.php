<?php

$params = array();

$params['alias']          = isset( $_REQUEST['alias'] ) ? $_REQUEST['alias'] : "";
$params['oauth_token']    = isset( $_REQUEST['oauth_token'] ) ? $_REQUEST['oauth_token'] : "";
$params['oauth_verifier'] = isset( $_REQUEST['oauth_verifier'] ) ? $_REQUEST['oauth_verifier'] : "";

// Set the database name that will be used for the entire session
define('DB_NAME', 'etrade');

// If this file is being visited by a human or Etrade's API
if( $params['alias'] <> "" || ( $params['oauth_token'] <> "" &&  $params['oauth_verifier'] <> "" ) )
{
  $etrade = new etrade();

  $request['request'] = 'generate access token';
  $request['human'] = 'visitor';
  $request['alias'] = $params['alias'];
  $request['oauth_token'] = $params['oauth_token'];
  $request['oauth_verifier'] = $params['oauth_verifier'];
  $request['token'] = $params['token'];

  $etrade->run_request( $request, "" );
}


class etrade
{
  private $account, $auth, $db, $debug, $display_debugging_messages, $request, $response;


  public function run_request( $request, $db='' )
  {
    // Include the required classes
    $this->__include();

    // Make sure all required variables are set

    $this->request = $request;
    //var_dump( $this->request ); die();

    $this->setup_database( $db );
    // var_dump( $this->db ); var_dump( $this->debug ); die();

    // Fetch the account
    $this->account = new account( $this->request, $this->db );
    // var_dump( $this->account ); die();

    $this->auth = new auth( $this->request, $this->account, $this->db );
    // var_dump( $this->auth ); die();

    $this->route_request();
    // var_dump( $this->response ); die();

    // Display debugging message (if set above)
    // $this->display_debugging();

    // echo "returning<br> ";

    // Return the raw information. The information will be validated in jesus.php
    // return $this->$response;

    return $this->response;
  }



  private function debug_array( $array )
  {
    // Convert the array into a string . . .
    $str = print_r( $array, TRUE );

    // Remove all the annoying formatting that <pre> displays
    $str = str_replace( "\n ", '', $str );
    $str = str_replace( "\n", '', $str );
    $str = str_replace( "  ", '', $str );

    return $str;
  }



  private function display_debugging()
  {
    // Add debug messages to the response
    if( $this->debug )
    {
      $this->response['debug'] =  $this->debug;
    }

    // Add error messages to the response
    if( $this->error )
    {
      $this->response['error'] = $this->error;
    }

    if( $this->display_debugging_messages === 'yes' )
    {
      echo "<pre>";
      echo "<hr><b>\$this->request (or fake \$this->request)</b><br><br>";
      print_r( $this->request );

      echo "<hr><b>\$this->response (or fake \$this->response)</b><br><br>";
      print_r( $this->response );
    }
  }



  private function route_request()
  {
    /*
     * CONCEPT:
     * broker->testbroker->route_request() routes the request to the appropriate class and file.
     *
     * CAUTION:
     */

    // print_r( $this->request ); die();
    // print_r( $this->response ); die();
    if( !empty( $this->account->recall ) )
    {
      $this->response = $this->account->recall;
      $this->response['warning'] = $this->account->debug['warning'][0];
    }
    elseif( !empty( $this->account->debug['warning'] ) )
    {
      $this->response['warning'] = $this->account->debug['warning'][0];
    }
    elseif( $this->request['request'] === 'get broker status' )
    {
      $this->debug[] = 'shrine->route_request() get broker status';

      // Instantiate the class that gets the account's broker status
      $status = new status($this->account,$this->auth,$this->request);

      $this->response = $status->get_status();
    }
    elseif( $this->request['request'] === 'get quotes' )
    {
      $this->debug[] = 'shrine->route_request() get quotes';

      // Instantiate the class that handles the quotes
      $quotes = new quotes($this->account,$this->auth,$this->request);

      $this->response = $quotes->get_quotes();
      // echo 'eeee'; var_dump( $this->response ); die();
    }
    elseif( $this->request['request'] === 'order' )
    {
      $this->debug[] = 'shrine->route_request() order';

      $this->debug[] = $this->request;

      // Instantiate the class that handles the orders
      $order = new order($this->account,$this->auth,$this->request);

      $this->response = $order->Place_Order();
    }
  }



  public function setup_database( $db )
  {
    // If the database connection has not already be set up
    if( !isset( $db ) || empty( $db ) )
    {
      // Include a list of the usernames passes and respective brokers
      

      // Include the database setup code
      require_once( dirname(__FILE__) . "/../lib/db_setup.php" );

      // Instantiate the class than handles the database setup and connection
      $db_setup = new db_setup();

      // Connect to the database and setup a database for the broker
      list( $this->db, $debug ) = $db_setup->build( $this->request );

      $this->debug[] = $debug;
    }
    else
    {
      // Otherwise use the db that was passed in by shrine/index.php
      $this->db = $db;
    }

    // var_dump( $this->db ); var_dump( $this->debug ); die();
  }



  private function __include()
  {
    // IMPORTANT: Include this Etrade config.php here (before Common/Common.php )
    require_once( dirname(__FILE__) . "/../config.php");
    require_once( dirname(__FILE__) . '/SDK/config.php');
    require_once( dirname(__FILE__) . '/../lib/db_setup.php');
    require_once( dirname(__FILE__) . '/account.php');
    require_once( dirname(__FILE__) . '/auth.php');
    require_once( dirname(__FILE__) . '/status.php');
    require_once( dirname(__FILE__) . '/quotes.php');
    require_once( dirname(__FILE__) . '/order.php');

    $this->request = array();
    $this->db = array();
    $this->debug = array();
  }


}


?>
