<?php


class order
{
  private $request;
  private $account;
  private $response;
  private $db;
  private $auth;
  private $AccountId;


  public function __construct( $account, $auth, $request )
  {
    /*
     * CONCEPT:
     * 1. Place an order using the request array
     * 2. Get the immediate response (that shows the order was successfully placed).
     * 3. Return the formatted response
     *
     * See page 151 in the Etrade API Documentation.
     *
     * CAUTION:
     * Etrade may have special rules for trading "penny stocks".  Follower may have to
     * calculate and place "limit orders" (rather than market orders) for penny stocks.
     *
     *
     * REQUEST:
     * $this->request = array(
     *   'request' => 'order'
     *   'account' => 'drew'
     *   'spirit_time' = '2013-08-01 03:09:00',
     *   'id' = 'DFJF8I0',
     *   'order' => array (
     *     array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => '1278', 'price' => '0.77' ),
     *     array( 'symbol' => 'GLYE', 't_type' => 'buy', 'shares' => '920', 'price' => '1.07' ),
     *     array( 'symbol' => 'KOOL' 't_type' => 'buy', 'shares' => '911', 'price' => '1.08' ) ) );
     *
     * RESPONSE:
     * $this->response = array(
     * 'account' => 'drew',
     * 'spirit_time' => '2013-08-01 03:09:00',
     * 'id' => 'DFJF8I0',
     * 'successful broker orders' => array(
     * array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => 101 ),
     * array( 'symbol' => 'RHAT', 't_type' => 'buy', 'shares' => 201 ),
     * array( 'symbol' => 'FMCC', 't_type' => 'buy', 'shares' => 301 ) ) );
     */

    $this->__include();

    $this->request = $request;
    $this->account = $account;
    $this->auth = $auth;

    /*
    $this->response = array(
      'spirit_time' => $this->request["spirit_time"],
      'id' => $this->request["id"] );
     */

    //$this->db = $db;
  }



  public function Place_Order()
  {
    $account = new etAccounts( $this->auth->et_consumer );

    $actlist_json = $account->GetAccountList();

    // decode the json string to array
    $act_response = json_decode( $actlist_json, TRUE );

    // to get the account id from the acount list response
    $act_list = $act_response["json.accountListResponse"]["response"];

    // 2DO the account id must come from the database.
    // The "cash" value below will change.
    // It is smart to make sure the accountID comes back in the list account,
    // but that should be checked earlier by status.php
    // to get the first CASH account ID
    foreach( $act_list AS $act )
    {
      if ( $act["marginLevel"] == "CASH")
      {
        $this->AccountId = $act["accountId"];
        break;
      }
    }

    $orderClient = new OrderClient( $this->auth->et_consumer );

    $orderList = array();

    foreach( $this->request["order"] AS $order )
    {
      $request_object = new EquityOrderRequest();

      $request_object->__set("symbol", $order["symbol"] );
      $request_object->__set("orderAction", strtoupper( $order["t_type"] ) );
      $request_object->__set("accountId", $this->AccountId );

      // 2DO we need to make sure that we can place orders for share that are less than $1.00
      // That may include changeing the following from "MARKET" to "LIMIT"
      $request_object->__set("priceType", "LIMIT" );
      $request_object->__set("quantity", $order["shares"] );
      $request_object->__set("limitPrice", $order["price"]);
      $request_object->__set("orderTerm", "GOOD_UNTIL_CANCEL");
      $request_object->__set("clientOrderId", $order["symbol"] . 'ID' . $this->request["id"] );
      $request_object->__set('previewId', '' );
      $request_object->__set('allOrNone', '' );
      $request_object->__set('reserveOrder', '' );
      $request_object->__set('reserveQuantity', 0 );
      $request_object->__set('stopLimitPrice', '' );
      $request_object->__set('routingDestination', '' );
      $request_object->__set('marketSession', 'REGULAR' );
      $request_object->__set('stopPrice', '' );
      try{
        //print "<br>START DEBUG DATA<br>";
        //print_r($request_object);
        $request_xml_object = new PlaceEquityOrder( $request_object );
        //print "<br>DEBUG DATA - request_xml_object<br>";
        //print_r($request_xml_object);
        $response_json = $orderClient->placeEquityOrder( $request_xml_object );
        //print "<br>DEBUG DATA - response_json<br>";
        //print_r($response_json);
        $response_arr = json_decode( $response_json, TRUE );
        //print "<br>DEBUG DATA - response_json<br>";
        //print_r($response_arr);
        //print "<br>END DEBUG DATA<br>";
        array_push( $orderList, array(
        'symbol' => $response_arr["PlaceEquityOrderResponse"]['EquityOrderResponse']["symbol"],
        't_type' =>$response_arr["PlaceEquityOrderResponse"]['EquityOrderResponse']["orderAction"],
        'shares' => $response_arr["PlaceEquityOrderResponse"]['EquityOrderResponse']["quantity"] ) );
        
      }catch(ETWSException $e){
        print "<br>DEBUG DATA - ETWSException<br>";
        print_r($e);
        
	$code = $e->getErrorCode();
        $message = $e->getErrorMessage();
        $this->debug[] = "EquityOrderRequest() error message [ $message ] code [ $code ] ";
      }catch(Exception $e){
        print "<br>DEBUG DATA - Exception<br>";
        print_r($e);
	
        $code = $e->getErrorCode();
        $message = $e->getErrorMessage();
        $this->debug[] = "EquityOrderRequest() error message [ $message ] code [ $code ] ";

      }
      
      
    }

    $this->response["successful broker orders"] = $orderList;

    return $this->response;
  }



  private function __include()
  {
    require_once(dirname(__FILE__) . '/../config.php');
    //require_once(dirname(__FILE__) . '/../lib/database.php');
    require_once(dirname(__FILE__) . '/auth.php');
    require_once(dirname(__FILE__) . '/account.php');
    require_once(dirname(__FILE__) . "/SDK/Orders/OrderClient.class.php");
    require_once(dirname(__FILE__) . "/SDK/Accounts/etAccounts.class.php");
  }


}


?>
