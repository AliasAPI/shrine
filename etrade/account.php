<?php


class account
{
  private $id;
  private $alias;
  private $account_id;
  private $oauth_consumer_key;
  private $oauth_consumer_secret;

  public $oauth_token;
  public $oauth_token_secret;

  public $oauth_verifier;
  public $timestamp; // Timestamp when the the oauth_verifier was created

  public $access_token;
  public $access_token_secret;

  private $received = NULL;
  private $response;
  private $human;

  private $debug = array();

  // The following elements get unset
  private $request;
  private $db;
  private $db_account;


  public function __construct( $request, $db )
  {
    $this->__include();

    $this->request = $request;
    $this->db = $db;

    // print_r( $this->request );
    $this->record_oauth_verifier();

    $this->fetch_account();

    $this->settings();

    $this->check_account();

    unset( $this->request );
    unset( $this->db );
    unset( $this->db_account );
  }



  private function check_account()
  {
    // print_r( $this->request ); die();

    // If there is no alias in the database
    if( !$this->request['alias']  )
    {
      $this->debug['warning'][] = 'The alias was not set.';
    }
    // If the alias was set, but it is NOT in the database
    elseif( isset( $this->request['alias'] ) && empty( $this->alias ) )
    {
      $this->debug['warning'][] = "The alias [ " . $this->request['alias'] . " ] was not found in the etrade.accounts table.";
    }
    // If there is no account_id in the database
    elseif( !$this->account_id || $this->account_id === NULL )
    {
      $this->debug['warning'][] = "The account_id was not found in the etrade.accounts table.";
    }
    // If there is no oauth_consumer_key in the database
    elseif( !$this->oauth_consumer_key || $this->oauth_consumer_key === NULL )
    {
      $this->debug['warning'][] = 'The oauth_consumer_key was not found in the etrade.accounts table.';
    }
    // If there is no oauth_consumer_secret in the database
    elseif( !$this->oauth_consumer_secret || $this->oauth_consumer_secret === NULL )
    {
      $this->debug['warning'][] = "The oauth_consumer_secret was not found in the etrade.accounts table.";
    }
    // If the tracking id is missing or NULL
    elseif( ( !isset( $this->request['id'] ) || empty( $this->request['id'] ) ) && $this->request['human'] !== 'visitor' )
    {
      $this->debug['warning'][] = "The tracking id is not set correctly.";
    }
    // If the request has already been  received ( and processed )
    elseif(!empty( $this->request['id'])&& !empty( $this->received ) &&  strpos( $this->received, $this->request['id'] ) !== FALSE )
    {
      // 2DO convert received into an array . . .  label it account->recall
      // print_r( $this->received ); die();
      // Just return $account->received back up to shrine/index.php
      $this->debug['warning'][] = "The request [ " . $this->request['id'] . " ] was already received.";
    }
    // If the user needs to MANUALLY authorize shrine
    elseif( ( !$this->oauth_token || $this->oauth_token === NULL
      || !$this->oauth_token_secret || $this->oauth_token_secret === NULL
      || !$this->oauth_verifier || $this->oauth_verifier === NULL
      || !$this->timestamp || $this->timestamp === '0000-00-00 00:00:00'
      || strtotime("+7 day", strtotime( $this->timestamp ) ) <= strtotime( date('Y-m-d H:i:s') ) )
      // Only return this warning if a human is not trying to authorize shrine
      && $this->request['human'] !== 'visitor' )
    {
      $this->debug['warning'][] = "Please authorize shrine by visiting http://" . SHRINE_URL . "/etrade/?alias=" . $this->request['alias']  . " ";
    }
  }



  private function fetch_account()
  {
    $account = array();

    // Pull all of the information for the account
    if(
       isset($this->request['oauth_verifier']) &&
             $this->request['oauth_verifier']<>""
       ){
      list( $this->db_account ) = $this->db->GetAll("SELECT * FROM `accounts`
      WHERE `oauth_verifier` = '" . $this->request['oauth_verifier'] . "' LIMIT 1 " );
    }else{

    list( $this->db_account ) = $this->db->GetAll("SELECT * FROM `accounts`
      WHERE `alias` = '" . $this->request['alias'] . "' LIMIT 1 " );
    }

    // var_dump( $this->db_account ); die();
    return $account;
  }



  public function record_access_token_and_secret( $account )
  {
    //  print_r( $this->request ); die();
    // If etrade sent the oauth_token and oauth_verifier
    if( !empty( $this->oauth_verifier ) && !empty( $account['access_token'] ) && !empty( $account['access_token_secret'] ) )
    {
      $this->db->Execute("UPDATE `accounts` SET
        `access_token` = '" . $account['access_token'] . "',
        `access_token_secret` = '" . $account['access_token_secret'] . "'
        WHERE `oauth_verifier` = '" . $this->oauth_verifier . "' ");


      // Set the access_token and access_token_secret for the accounts object
      list( $this->access_token, $this->access_token_secret ) = $this->db->GetAll("SELECT `access_token`, `access_token_secret` FROM `accounts`
        WHERE `oauth_verifier` = '" . $this->oauth_verifier . "' LIMIT 1 ");

      // Here we check to make sure the values were stored correctly
      if( $this->access_token === $account['access_token'] && $this->access_token_secret == $account['access_token_secret'] )
      {
        return TRUE;
      }
      else
      {
        return FALSE;
      }
    }
    else
    {
      return FALSE;
    }
  }



  private function record_oauth_verifier()
  {
    //  print_r( $this->request ); die();

    // If etrade sent the oauth_token and oauth_verifier
    if( !empty( $this->request['oauth_token'] ) && !empty( $this->request['oauth_verifier'] ) )
    {
      list( $token_it ) = $this->db->GetAll("SELECT `oauth_token` FROM `accounts`
        WHERE `oauth_token` = '" . $this->request['oauth_token'] . "' LIMIT 1 ");

      if( $token_it )
      {
        $this->db->Execute("UPDATE `accounts` SET
          `oauth_verifier` = '" . $this->request['oauth_verifier'] . "',
          `timestamp` = NOW()
          WHERE `oauth_token` = '" . $this->request['oauth_token'] . "' ");

        list( $verify_it ) = $this->db->GetAll("SELECT `oauth_verifier` FROM `accounts`
          WHERE `oauth_token` = '" . $this->request['oauth_token'] . "' LIMIT 1 ");

        if( $verify_it )
        {
          echo "The oauth_verifier = [ " . $this->request['oauth_verifier'] . " ]
            has been stored for oauth_token =  [ " . $this->request['oauth_token'] . " ]
            Thank you!";
        }
      }
      else
      {
        echo "<center>The oauth_token [ " . $this->request['oauth_token'] . " ] is not in the etrade.accounts table.</center>";
      }
    }
  }



  private function settings()
  {
    if( isset( $this->db_account ) )
    {
      foreach( $this->db_account AS $key => $value )
      {
        $this->$key = $value;
      }
    }

    if( isset($this->request['human']) && $this->request['human'] === 'visitor' )
    {
      $this->human = 'visitor';
    }
    // var_dump( $this ); die();
  }



  public function __get( $property )
  {
    if( property_exists( $this, $property ) )
    {
      return $this->$property;
    }
  }



  private function __include()
  {
    // $this->db = $db;
    // var_dump( $this->db ); die();

    $this->request = array();
    $this->db = array();
    $this->debug = array();
  }



  public function __set( $property, $value )
  {
    if( property_exists( $this, $property ) )
    {
      $this->$property = $value;
    }

    return $this;
  }


}


?>
