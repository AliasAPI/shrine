
  VERIFY SHRINE (WITH MANUAL.PHP)
  . Call http://etrade-shrine<number>.rhcloud.com/etrade/test/run.php?filter=01
  . Call each unit_test by modifying the last number 01, 02, 03, 04, 05, 06
  . To get the $oauth_verifier please run test 07:
  . Call http://etrade-shrine<number>.rhcloud.com/etrade/test/run.php?filter=07
  . Please enter the username and password.
  . Click "Accept"
  . That will redirect to a URL with an oauth_verifer at the end.
  . Copy the part of the URL from the "?" to the end.
  . Add the string to the manual.php:
  . EXAMPLE: http://etrade-shrine.rhcloud.com/lib/manual.php?oauth_token=gqEtOUI%3D&oauth_verifier=6AQZV
  . The process will store the oauth_verifier in your database.
  . NOTE: Occasionally, you may have to do the process to get a new oauth_verifier.
  . Once you verify shrine, you can begin to develop and run the test you are assigned.


DEVELOPMENT
. Etrade SDK Techinical Documentation: shrine/etrade/documentation/etrade_API_Technical_Documentation.pdf
This open source project uses the Etrade PHP SDK to get information and place orders with the Etrade Broker.

. Old unit testing system shrine/lib/test_etrade.php
We have successfully completed the authorization process, retrieved data and placed orders in the sandbox
environment. You can use the old unit testing system to prove that most of the functionality works already.

. shrine/etrade/lib/db_config.php
If you want to work with your local server, modify the db_config.php file with your MySQL server settings.
Otherwise, you can use our ftp server (that already has the code and database set up). In either case, the
MySQL database and table are automatically set up when you run the code. (FTP password available on request)

. Send a $request array, get a $response array
The unit tests are simple. In each case, a $request array is sent. We expect a specific $response array to 
be returned. The unit test account will be located in the accounts table with the alias "testar". The pass
and key for the "etrade sandbox environment" are automatically stored in the `accounts` table.

. Run the unit tests:  
To run the unit tests on our server, there are choices:
A. http://etrade-shrine[your shrine number].rhcloud.com/test/run.php  (Your remote openshift shrine)
B. http://localhost/shrine/test/run.php (Your local server )
Please set the filter to the test you would like to run.  For example, if you want to run unit test 01_XXX
then use http://localhost/shrine/test/run.php?filter=01 

. Goal
The goal of this project is to simply correct the code until your unit test passes. For unit tests 08 - 15
you will need to authorize shrine by passsing unit test 07. Then use lib/manual.php (as described above)
to store the oauth_verifier in the appropriate database.


. unit_tests . . .
01_alias_not_set_etrade.php :: This unit test already passes.
02_alias_not_in_database_etrade.php :: This unit test already passes.
03_oauth_consumer_key_not_in_database_etrade.php :: Establish a etrade database MySQL Connection
04_oauth_consumer_secret_not_in_database.php :: Establish a etrade database MySQL Connection
05_account_id_not_in_database_etrade.php :: Establish a etrade database MySQL Connection
06_authorize_shrine_etrade.php :: The $response should let the user know to authorize shrine
07_get_oauth_verifier_etrade.php :: Etrade will send the oauth_verifier to OUR server. 
08_get_broker_status_etrade.php :: This test requires the authorization and oauth_verifier. 
09_get_quotes_etrade.php :: This test will easily pass once the 08 test passes.
10_execute_order_etrade.php :: Create the alerts for the remaining tests.
11_get_order_status_etrade.php :: A duplicate of the 08 test that checks for more results.
12_alerts_get_returned_etrade.php :: Pull the alerts. Alerts are used for order tracking.
13_alerts_get_deleted.php :: Delete alerts (to help limit the size of the $response array).
14_alerts_get_current_id_etrade.php :: Get the previous alert id
15_alerts_id_saved_etrade.php :: Store the new max alert id
broker.php :: Switch from the etrade class and run an unit test for a test broker

Once all of the unit tests pass easily in the sandbox environment, we will change the passwords
and run the same tests in the Etrade Production enviroment. The production environment uses real
money. I will provide the production passwords after reviewing the successful sandbox unit tests.




