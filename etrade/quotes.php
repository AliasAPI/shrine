<?php


class quotes
{
  private $account;
  private $request;
  private $response;
  private $consumer;
  private $auth;
  private $db;


  public function __construct( $account, $auth, $request )
  {
    /*
     * CONCEPT:
     *
     * CAUTION:
     *
     *
     * REQUEST:
     * $request = array(
     * ' request' => 'get quotes',
     * 'alias' => 'quotes',
     * 'get quotes' = array('ASTC', 'FMCC', 'FNMA'),
     * 'spirit_time' => '2013-10-01 10:10:00',
     * 'id' => 'DFJF8I0' );
     *
     * $response = array( 'alias' => string 'testar',
     * 'spirit_time' => string '2013-06-26 03:06:27',
     * 'id' => string 'TRACKIT',
     * 'quotes' => array(
     *   'GOOG' => string '24.63',
     *   'AAPL' => string '15.15',
     *   'MSFT' => string '1.81' );
     */


    $this->account = $account;
    $this->auth = $auth;
    $this->request = $request;

    $this->__include();
  }



  public function get_quotes()
  {
    try {
      // Instantiate the Market Class
      $MarketClient = new MarketClient( $this->auth->et_consumer );

      $request_params = new getQuoteParams();

      $request_params->__set('symbolList', $this->request['get quotes'] );     // symbolList = GOOG for example
      $request_params->__set('detailFlag', 'FUNDAMENTAL'); 	 // detailFlag = WEEK_52 for example
      //print "<br>DEBUG DATA<br>";
      //print_r($request_params);
      $response_json = $MarketClient->getQuote( $request_params );
      //print("<br>Response:<br>" . $response_json);
      //print "<br>END DEBUG DATA<br>";
      
      
    }
    catch( ETWSException $e )
    {
      echo 	"***Caught exception***  \n".
        "Error Code 	: " . $e->getErrorCode()."\n" .
        "Error Message 	: " . $e->getErrorMessage() . "\n" ;
      if(DEBUG_MODE) echo $e->getTraceAsString() . "\n" ;
      exit;
    }
    catch( Exception $e )
    {
      echo 	"***Caught exception***  \n".
        "Error Code 	: " . $e->getCode()."\n" .
        "Error Message 	: " . $e->getMessage() . "\n" ;
      if(DEBUG_MODE) echo $e->getTraceAsString() . "\n" ;
      echo "Exiting...\n";
      exit;
    }

    $response_obj = json_decode( $response_json, TRUE );

    $this->response['quotes'] = array();

    foreach( $response_obj["quoteResponse"]["quoteData"] as $quote )
    {
      if($quote['errorMessage']==="Invalid Symbol"){
        //print "Invalid Symbol " . $quote["product"]["symbol"] . "<br>";
      }else{
        $this->response['quotes']["" . $quote["product"]["symbol"] . ""] = "" . $quote['fundamental']['lastTrade'] . "";
      }
    }

    $this->response['id']=$this->request['id'];

    return $this->response;
  }



  private function __include()
  {
    require_once( dirname(__FILE__) . '/account.php');
    // Rajesh shrine/etrade/index.php includes the /shrine/etrade/SDK/config.php instead
    require_once 'SDK/config.php';
    // Rajesh Here is your Common/Common
    require_once 'SDK/Common/Common.php';
    require_once( dirname(__FILE__) . '/SDK/Market/MarketClient.class.php');
    require_once( dirname(__FILE__) . '/SDK/Market/getQuoteParams.class.php');

    $this->account = array();
    $this->consumer = array();
    $this->db = array();
  }


}


?>
