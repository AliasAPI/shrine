<?php


class auth
{
  private $et_consumer;
  private $et_oauth;
  private $oauth_req;
  private $marketClient;
  private $orderClient;
  public  $response;
  private $request;
  private $debug;

  // These get unset
  private $auth_url;
  private $account;
  private $accountVars;
  private $db;
  private $option;


  public function __construct( $request, $account, $db )
  {
    // account.php and auth.php will ALWAYS run before status.php, quotes.php, order.php

    // var_dump( $account->debug['warning'] ); die();

    // 2DO add the db check  too


    $ts  = new DateTime( $account->timestamp );
    $now = new DateTime( );
    $diff = $ts->diff($now);
    $hrs = $diff->format( '%H' );
    if( $hrs < 2 )
    {
      //print "DEBUG: No Token renewal required now<br>";
      $renewToken =0;
    }
    else
    {
      //print "DEBUG: Renewing token<br>";
      $renewToken =1;
    }


    if( $account->human === 'visitor' || empty( $account->debug['warning'] ) || $renewToken == 1 )
    {
      $this->__include();

      $this->account = $account;
      // var_dump( $this->account ); die();

      $this->db = $db;

      $this->get_consumer();

      $this->get_oauth();

      // IF there is not verifier???
      // IF verifier present don't do anything here, just save it to DB
      // IF generating permanent token

      if( isset( $request['human'] ) && $request['human'] === 'visitor'
        && isset( $request['oauth_verifier'] ) && $request['oauth_verifier'] === '' )
      {
        $this->get_tokens();

        $this->record_tokens();

        $this->get_auth_url();

        $this->direct_human();
      }
      if( $renewToken == 1 )
      {
        $this->renew_access_token();
      }
      if( isset( $request['human'] ) && $request['human'] === 'visitor'
        && isset( $request['oauth_verifier'] ) && $request['oauth_verifier'] <> '' )
      {
        // Access tokens expire after 2 hours . . .
        // I think the Etrade documentation recommends revoking access tokens after each use.
        $this->get_access();
        $this->record_access_token_and_secret();
      }

      // By this point, shrine should be fully authorized to manage the account.
      return $this;
    }
  }



  private function direct_human()
  {
    if( $this->account->human === 'visitor' )
    {
      $this->debug[] = "direct_human()
        account->human [ " . $this->account->human . " ]
        auth_url [ " . $this->auth_url . " ] ";

      // var_dump( $this->account->human ); die();
      // var_dump( $auth_url ); die();

      echo "<br><br><br><center><table><tr><td>";
      echo "1. <a href=$this->auth_url><font color=#000000>CLICK HERE</font></a> to go to Etrade.<br>";
      echo "2. Manually enter your Etrade username and password.<br>";
      echo "3. Once logged in, click the Accept button.<br>";
      echo "</td></tr></table></center>";

      die();
    }
  }



  private function get_access()
  {
    // 2DO No human
    // var_dump( $this ); echo "<hr>"; var_dump( $this->account->oauth_verifier );  die();

    $this->et_oauth->oauth_token = $this->account->oauth_token;
    $this->et_oauth->oauth_token_secret = $this->account->oauth_token_secret;

    try {
      // This class is in ./OAuth/etOAuth.class.php
      $access_token = $this->et_oauth->GetAccessToken( $this->account->oauth_verifier );

      $this->accountVars['oauth_verifier'] = $this->account->oauth_verifier;
      $this->accountVars['access_token'] = $access_token['oauth_token'];
      $this->accountVars['access_token_secret'] = $access_token['oauth_token_secret'];

      $this->debug[] = "access_token = [ " . $this->accountVars['access_token'] . " ]
        access_token_secret = [ " . $this->accountVars['access_token_secret'] . " ] ";
    }
    catch( OAuthException $e1 )
    {
      $code1 = $e1->getCode();
      $message1 = $e1->getMessage();
      $this->debug[] = "get_access_token() error message1 [ $message1 ] code1 [ $code1 ] ";
    }
    // catch for ValidateRequiredParams
    catch( ETWSException $e2 )
    {
      $code2 = $e2->getCode();
      $message2 = $e2->getMessage();
      $this->debug[] = "get_access_token() error message2 [ $message2 ] code2 [ $code2 ] ";
    }
  }



  private function get_auth_url()
  {
    if( $this->account->human === 'visitor' )
    {
      try {
        $this->auth_url = $this->et_oauth->GetAuthorizeURL();
      }
      catch( OAuthException $e )
      {
        $this->debug['emergency'][] = $e->getCode();
        $this->debug['emergency'][] = $e->getMessage();
      }

      $this->debug[] = "get_auth_url()
        account->human [ " . $this->account->human . " ]
        auth_url [ " . $this->auth_url . " ] ";
    }
  }



  private function get_consumer()
  {
    try {
      $this->et_consumer = new etOAuthConsumer( $this->account->oauth_consumer_key, $this->account->oauth_consumer_secret );
      $this->et_consumer->oauth_token	= $this->account->access_token;
      $this->et_consumer->oauth_token_secret = $this->account->access_token_secret;
    }
    catch( OAuthException $e )
    {
      $this->debug['emergency'][] = $e->getCode();
      $this->debug['emergency'][] = $e->getMessage();
    }

    $this->debug[] = "get_consumer()
      etOAuthConsumer->key [ " . $this->et_consumer->key . " ]
      etOAuthConsumer->secret [ " . $this->et_consumer->secret . " ] ";
  }



  private function get_oauth()
  {
    // var_dump( $this->account ); die();
    // var_dump( $this->et_consumer ); die();

    try {
      $this->et_oauth = new etOAuth( $this->et_consumer );
    }
    catch( OAuthException $e )
    {
      $this->debug['emergency'][] = $e->getCode();
      $this->debug['emergency'][] = $e->getMessage();
    }

    $this->debug[] = "get_oauth()
      etOAuth->key [ " . $this->et_oauth->key . " ]
      etOAuth->secret [ " . $this->et_oauth->secret . " ]
      etOAuth->callback_url [ " . $this->et_oauth->callback_url . " ] ";
  }



  private function get_tokens()
  {
    // Get the 'oauth_token' and 'oauth_token_secret'

    try {
      $request_tokens =  $this->et_oauth->GetRequestToken();
      $this->debug[] = "get_tokens()
        etOAuth->oauth_token [ " . $this->et_oauth->oauth_token . " ]
        etOAuth->oauth_token_secret [ " . $this->et_oauth->oauth_token_secret . " ]
        etOAuth->oauth_callback_confirmed [ " . $request_tokens['oauth_callback_confirmed'] . " ] ";
    }
    catch( OAuthException $e )
    {
      $this->debug['emergency'][] = $e->getCode();
      $this->debug['emergency'][] = $e->getMessage();
    }
    catch( ETWSException $eT )
    {
      $this->debug['emergency'][] = $eT->getCode();
      $this->debug['emergency'][] = $eT->getMessage();
    }
  }



  private function record_access_token_and_secret()
  {
    $recorded = FALSE;
    $this->account->db = $this->db;
    $recorded = $this->account->record_access_token_and_secret( $this->accountVars );

    return $recorded;
  }



  private function record_tokens()
  {
    if( $this->account->human === 'visitor' )
    {
      $update_q = "UPDATE `accounts` SET
        `oauth_token` = '" . $this->et_oauth->oauth_token . "',
        `oauth_token_secret` = '" . $this->et_oauth->oauth_token_secret . "',
        `received` = 'A human was here.'
        WHERE `account_id` = '" . $this->account->account_id . "' ";
    }
    else
    {
      $update_q = "UPDATE `accounts` SET
        `oauth_token` = '" . $this->et_oauth->oauth_token . "',
        `oauth_token_secret` = '" . $this->et_oauth->oauth_token_secret . "',
        `received` = 'A human was here.'
        WHERE `account_id` = '" . $this->account->account_id . "' ";
    }

    $this->db->Execute("UPDATE `accounts` SET
      `oauth_token` = '" . $this->et_oauth->oauth_token . "',
      `oauth_token_secret` = '" . $this->et_oauth->oauth_token_secret . "',
      `received` = 'A human was here.'
      WHERE `account_id` = '" . $this->account->account_id . "' " );

    $this->debug[] = "record_tokens() $update_q";
  }



  private function renew_access_token()
  {
    $response = array();

    $this->et_oauth->oauth_token = $this->account->access_token;
    $this->et_oauth->oauth_token_secret = $this->account->access_token_secret;

    try {
      $retStr = $this->et_oauth->RenewAccessToken();

      if( $retStr == "Access Token has been renewed" )
      {
        $this->db->Execute("UPDATE `accounts` SET
          timestamp=now()
          WHERE `account_id` = '" . $this->account->account_id . "' " );
      }
    }
    catch( OAuthException $e )
    {
      $response["errorCode"] = $e->getCode();
      $response["errorMsg"] = $e->getMessage();
    }
    catch( ETWSException $e )
    {
      $response["errorCode"] = $e->getCode();
      $response["errorMsg"] = $e->getMessage();
    }

    return $response;
  }



  private function revoke_access_token()
  {
    $response = array();

    try {
      $response = $this->oauth_req->RevokeAccessToken();
    }
    catch( OAuthException $e )
    {
      $response["errorCode"] = $e->getCode();
      $response["errorMsg"] = $e->getMessage();
    }
    catch( ETWSException $e )
    {
      $response["errorCode"] = $e->getCode();
      $response["errorMsg"] = $e->getMessage();
    }

    return $response;
  }



  public function __get($property)
  {
    if( property_exists( $this, $property ) )
    {
      return $this->$property;
    }
  }



  private function __include()
  {
    require_once( dirname(__FILE__) . '/account.php');
    require_once( dirname(__FILE__) . '/SDK/Common/Common.php');
    require_once( dirname(__FILE__) . '/SDK/OAuth/OAuth.php');
    require_once( dirname(__FILE__) . '/SDK/OAuth/etOAuth.class.php');
    require_once( dirname(__FILE__) . '/SDK/OAuth/etOAuthConsumer.class.php');
    require_once( dirname(__FILE__) . '/SDK/OAuth/etOAuth.class.php');

    $this->account = array();
    $this->consumer = array();
    $this->db = array();
    // $this->account = array();
  }



  public function __set( $property, $value )
  {
    if( property_exists( $this, $property ) )
    {
      $this->$property = $value;
    }

    return $this;
  }


}


?>
