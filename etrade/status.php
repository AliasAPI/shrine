<?php


class status
{
  private $request;
  private $db;
  private $debug;
  private $auth;
  private $account;
  private $response;
  private $AccountId;


  public function __construct( $account, $auth, $request )
  {
    /*
     * CONCEPT:
     * shrine/status.php retrieves the current status information from the broker.
     * Some brokers (like just2trade) automatically send back all of the information
     * immediately. If that happens, the attributes are sent to other functions to be
     * split up and returned to jesus. Otherwise, the functions pull the information.
     * The entire class is meant to compile a 'response' array to be sent to shrine
     *
     *
     * CAUTION:
     * Use minimal error checking and data validation. jesus should validate the data.
     *
     *
     * REQUEST:
     * $request = array(
     *  'request' => 'get broker status',
     *  'alias' => 'drew',
     *  'spirit_time' => '2013-10-01 10:10:00',
     *  'id' => 'DFJF8I0' );
     *
     *
     * NORMAL RESPONSE:
     * $this->response = array(
     *  'alias' => 'drew',
     *  'spirit_time' => '2013-08-01 03:09:00',
     *  'id' => 'DFJF8I0',
     *  'broker status' => array(
     *  'open_market' => 'yes',
     *  'cash_broker' => '3003',
     *  'margin' = 'yes',
     *  'broker_positions' => array(
     *    array( 'symbol' => 'ASTC', 'shares' => 101 ),
     *    array( 'symbol' => 'RHAT', 'shares' => 201 ),
     *    array( 'symbol' => 'FMCC', 'shares' => 301 ) ),
     *  'done_orders' => array(
     *    array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => 50, 'price' => 1.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'AKFJD9393'),
     *    array( 'symbol' => 'RHAT', 't_type' => 'buy', 'shares' => 201, 'price' => 2.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'BKFJD9393' ),
     *    array( 'symbol' => 'FMCC', 't_type' => 'buy', 'shares' => 301, 'price' => 3.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'CKFJD9393' ) ),
     *  'open_orders' => array('symbol' => 'ASTC', 'shares' => 51 ) ) );
     */


    $this->__include();

    $this->request = $request;
    $this->account = $account;

    $this->auth = $auth;

    // var_dump( $this->request ); var_dump( $this->response ); var_dump( $this->auth ); die();

    // Check if it is a margin or cash account
    $this->check_margin();

    // Check to see if the stock markets are open
    $this->check_markets();

    // Get the current cash balance
    $this->get_cash_broker();

    //var_dump( $this->response ); die();
    // Get the broker_positions to get the number of shares owned for each symbol
    $this->get_broker_positions();

    // Get the new done_orders
    //$this->get_done_orders();

    // Get the number of shares owed in the future for each symbol
    $this->get_open_done_orders();


    return $this->response['broker status'];
  }



  private function check_margin()
  {
    /*
     * CONCEPT:
     * 1. Once the token is stored in the database, it is used to get a final username and password
     * 2. The final username and password is combined with a nonce, timestamp and other attributes.
     * 3. Once it has all of the credentials, shrine is able to log into the Etrade account API.
     * 4. The goal is to return get the response that indicates that shrine has logged into it.
     *
     * I am not sure what response shows a successful login in the Etrade API documentation.
     *
     * CAUTION:
     *
     * REQUEST:
     * $this->request = array(
     *  'request' => 'get broker status',
     *  'account' => 'drew',
     *  'spirit_time' => '2013-10-01 10:10:00',
     *  'id' => 'DFJF8I0' );
     *
     * RESPONSE:
     * Return 'logged_in' => 'yes' to $this->get_broker_status()
     *    $this->response['broker status']['logged_in'] = ( $this->request['demon']['logged_in'] )
     * ? $this->request['demon']['logged_in'] : '';
     */

    /*
     * IF FOLLOWER COULD NOT LOG INTO THE ACCOUNT
     * $this->response = array(
     *  'account' => 'drew',
     *  'spirit_time' => '2013-08-01 03:09:00',
     *  'id' => 'DFJF8I0',
     *  'warning' => 'Follower could not log into the account' );
     */

    // Set the default margin ( for cash accounts )
    $this->response['broker status']['margin'] = 'no';

    $this->account = new etAccounts( $this->auth->et_consumer );

    $actlist_json = $this->account->GetAccountList();

    // decode the json string to array
    $act_response = json_decode( $actlist_json, TRUE );

    if( is_array( $act_response ) )
    {
      // to get the account id from the acount list response
      $act_list = $act_response["json.accountListResponse"]["response"];

      // to get the first CASH account ID
      foreach( $act_list AS $act )
      {
        if( $act["marginLevel"] == "CASH" )
        {
          $this->AccountId = $act["accountId"];
          break;
        }
      }
    }
  }



  private function check_markets()
  {
    /*
     * CONCEPT:
     * open_market() retrieves the response that indicates that the stock market is open.
     * spirit will only send requests during the times the market should be open, but it
     * is still important to get an indication from the broker that the markets are open.
     *
     * CAUTION:
     * Sometimes the market closes halfway through the day or during an economic crisis.
     * I'm not sure what indicator shows the market is open in the Etrade API documentation.
     *
     *
     * REQUEST:
     * $this->request = array(
     *  'request' => 'get broker status',
     *  'account' => 'drew',
     *  'spirit_time' => '2013-10-01 10:10:00',
     *  'id' => 'DFJF8I0' );
     *
     * RESPONSE:
     * Return 'open_market' => 'yes' to $this->get_broker_status()
     *  $this->response['broker status']['open_market'] = ( $this->request['demon']['open_market'] )
     * ? $this->request['demon']['open_market'] : '';
     */

    // 2DO see CAUTION: above. Pretending the market is always open will trick spirit.
    $p = file_get_contents("http://finance.yahoo.com/");

    if( preg_match("/U.S. Markets closed/",$p)==1 ){
      $this->response['broker status']['open_market'] = 'no';
    }else{
      if( preg_match("/U.S. Markets are open/", $p )==1){
        $this->response['broker status']['open_market'] = 'yes';
      }else{
        $this->response['debug'] = "finance.yahoo.com changed the site";    
      }
    }
  }



  private function get_cash_broker()
  {
    /*
     * CONCEPT:
     * cash_broker() gets the amount of CASH in the account that is available.
     *
     * See "netCash" on page 49 in the Etrade API documentation.
     * See page 233 of the documentation
     *
     * CAUTION:
     * Do NOT get the cash amount that includes "margin". Follower needs cash.
     *
     *
     * REQUEST:
     * $this->request = array(
     *  'request' => 'get broker status',
     *  'account' => 'drew',
     *  'spirit_time' => '2013-10-01 10:10:00',
     *  'id' => 'DFJF8I0' );
     *
     * RESPONSE:
     * Return 'cash_broker' => '3931.25' to $this->get_broker_status()
     *     // ???+++ and if there are NO broker_positions (if broker_positions === NULL / '')
     * $this->response['broker status']['cash_broker'] = ( $this->request['demon']['cash_broker'] == 0 )
     * ? '3003' : $this->request['demon']['cash_broker'];
     */

    // var_dump( $this->auth->consumer ); die();
    $this->account = new etAccounts( $this->auth->et_consumer );

    //  var_dump( $this->account ); die();
    $this->response['debug'] = "get_cash_broker() with account_id [ " . $this->auth->account->account_id . " ] ";

    // var_dump( $this->auth );

    $act_response = $this->account->GetAccountBalance( $this->auth->account->account_id );

    //var_dump( $act_response ); die();
    $act = json_decode( $act_response );

    if( is_object( $act ) )
    {
      $act_arr  = get_object_vars($act);
      $response_obj = $act_arr["json.accountBalanceResponse"];

      $this->response['broker status']['cash_broker'] = round( floor( $response_obj->accountBalance->netCash * 100 ) / 100, 2);
    }
  }



  private function get_broker_positions()
  {
    /*
     * CONCEPT:
     * get_broker_positions() all of the symbols and the number of shares for each stock
     * owned in the brokerage account.  See page 54 of the Etrade API documentation.
     *
     * CAUTION:
     *
     *
     * REQUEST:
     * $this->request = array(
     *  'request' => 'get broker status',
     *  'account' => 'drew',
     *  'spirit_time' => '2013-10-01 10:10:00',
     *  'id' => 'DFJF8I0' );
     *
     * RESPONSE
     * Return the following array to $this->get_broker_status()
     * $broker_positions = array(
     *  array( 'symbol' => 'ASTC', 'shares' => 101 ),
     *  array( 'symbol' => 'RHAT', 'shares' => 201 ),
     *  array( 'symbol' => 'FMCC', 'shares' => 301 ) );
     *
     * If a request['done_orders'] has been sent, send it back
     * $this->response['broker status']['broker_positions'] = ( $this->request['demon']['broker_positions'] )
     * ? $this->request['demon']['broker_positions'] : NULL;
     */

    $request_param = new AccountPositionsRequest();

    $response_position = $this->account->GetAccountPositions( $this->AccountId, $request_param );

    $response_obj = json_decode( $response_position, TRUE );

    $broker_position = array();

    $pos = null;

    foreach( $response_obj["json.accountPositionsResponse"]["response"] AS $position )
    {
      $pos = array();
      $pos['symbol'] = $position["productId"]["symbol"];
      $pos['shares'] = $position["qty"];

      array_push( $broker_position, $pos );
    }

    $this->response['broker status']['broker_positions'] = $broker_position;
  }



  private function get_done_orders()
  {
    /*
     * CONCEPT:
     * done_orders() retrieves the data that shows what orders have been done.
     * Each done_order MUST include a unique identifier (Alert ID) so that the
     * rest of the system can function. Notice there is an "orderid" that goes
     * with each done_order. For now, etrade does not have LOT IDs, but we can
     * use "Alert IDs" instead.
     *
     * See pages 26 and 126 of the Etrade API Documentation.
     *
     *
     * CAUTION: The broker may have a temporary page that lists the recent subtransactions and disappears.
     * It is very important to get the information from the permanent page that shows the subtransactions.
     * All done orders must have a datetime stamp in this format: 'YYYY-MM-DD HH:MM:SS'
     *
     * Just2Trade sends "a summary of completed trades (both executed and cancelled for the current day"
     * "The trade summaries only show aggregated totals for positions bought and sold and prices. That is,
     * if an order was executed in more than one fill, only the total quantity executed and the average
     * price of all executions will be shown" . . . so . . .
     *
     * . The report is limited to whole transactions (in cumulative amounts until the total is returned)
     * . That means jesus will have to ignore the number of shares being too high
     * . It can cause confusion on orders that are not completely filled within the day
     *
     * RESPONSE:
     * Return the following array to $this->get_broker_status()
     * $done_orders => array(
     *    array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => 50, 'price' => 1.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'AKFJD9393'),
     *    array( 'symbol' => 'RHAT', 't_type' => 'buy', 'shares' => 201, 'price' => 2.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'BKFJD9393' ),
     *    array( 'symbol' => 'FMCC', 't_type' => 'buy', 'shares' => 301, 'price' => 3.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'CKFJD9393' ) );
     *    // If a request['done_orders'] has been sent, send it back
     */

    $orderClient = new OrderClient( $this->auth->et_consumer );

    $order_list = $orderClient->getOrderList( $this->AccountId );

    $order_obj = json_decode( $order_list, TRUE );

    $done_order = array();

    $order = null;

    foreach( $order_obj["GetOrderListResponse"]["orderListResponse"]["orderDetails"] AS $order_arr )
    {
      if( $order_arr["order"]["orderStatus"] == "EXECUTED" )
      {
        if ( strtolower($order_arr["order"]["legDetails"]["orderAction"]) == "sell"
          || strtolower($order_arr["order"]["legDetails"]["orderAction"]) == "buy" )
        {
          $order = array();

          $order['symbol'] = $order_arr["order"]["legDetails"]["symbolInfo"]["symbol"];
          $order['t_type'] = strtolower( $order_arr["order"]["legDetails"]["orderAction"] );
          $order['shares'] = $order_arr["order"]["legDetails"]["orderedQuantity"];
          $order['price'] = $order_arr["order"]["legDetails"]["executedPrice"];
          $order['date'] = date("Y-m-d H:i:s", strtotime($order_arr["order"]["orderExecutedTime"]) );
          $order['orderid'] = $order_arr["order"]["orderId"];

          array_push($done_order,$order);
        }
      }
    }

    $this->response['broker status']['done_orders'] = $done_order;
  }



  private function get_open_done_orders()
  {
    /*
     * CONCEPT:
     * get_open_orders() retrieves the number of shares that the broker owes shrine.
     * For example, an order to buy 7,000 shares of FMCC is placed. Etrade is able to
     * fill 4500 shares immediately. Etrade will also note that it plans to get the
     * remaining 2,500 shares.  See pages, 126 of the Etrade API Documentation.
     *
     * CAUTION:
     * The open_orders values are critical because the system audits the amount of
     * shares that are recorded locally versus the amount of shares the broker has
     * listed.
     *
     *
     * RESPONSE:
     * Return the following array to $this->get_broker_status()
     * $open_orders = array('symbol' => 'FMCC', 'shares' => '2500' );
     */

    $orderClient = new OrderClient( $this->auth->et_consumer );

    $order_list = $orderClient->getOrderList( $this->AccountId );

    $order_obj = json_decode( $order_list, TRUE );

    $open_order = array();
    $done_order = array();
    

    $order = null;

    foreach( $order_obj["GetOrderListResponse"]["orderListResponse"]["orderDetails"] AS $order_arr )
    {
      if( $order_arr["order"]["orderStatus"] == "OPEN" )
      {
        if ( strtolower($order_arr["order"]["legDetails"]["orderAction"]) == "sell"
          || strtolower($order_arr["order"]["legDetails"]["orderAction"]) == "buy" )
        {
          $order = array();

          $order['symbol'] = $order_arr["order"]["legDetails"]["symbolInfo"]["symbol"];
          $order['shares'] = $order_arr["order"]["legDetails"]["orderedQuantity"] - $order_arr["order"]["legDetails"]["filledQuantity"];
        
          //$order['t_type'] = strtolower( $order_arr["order"]["legDetails"]["orderAction"] );
          //$order['price'] = $order_arr["order"]["legDetails"]["executedPrice"];
          //$order['date'] = date("Y-m-d H:i:s", $order_arr["order"]["orderExecutedTime"] );
          //$order['orderid'] = $order_arr["order"]["orderId"];

        array_push( $open_order, $order );
        }
      }
      if( $order_arr["order"]["orderStatus"] == "EXECUTED" )
      {
        $order = array();
        $order['symbol'] = $order_arr["order"]["legDetails"]["symbolInfo"]["symbol"];
        $order['shares'] = $order_arr["order"]["legDetails"]["filledQuantity"];
        $order['t_type'] = $order_arr["order"]["legDetails"]["orderAction"];
        $order['price'] = $order_arr["order"]["legDetails"]["executedPrice"];
        $order['date'] = date("Y-m-d H:i:s", $order_arr["order"]["orderExecutedTime"] );
        $order['orderid'] = $order_arr["order"]["orderId"];
        array_push( $done_order, $order );
        
        
      }
    }
    


    $this->response['broker status']['open_orders'] = $open_order;
    $this->response['broker status']['done_orders'] = $done_order;
    
  }



  public function get_status()
  {
    return $this->response;
  }



  private function __include()
  {
    require_once(dirname(__FILE__) . '/../config.php');
    require_once(dirname(__FILE__) . '/auth.php');
    require_once(dirname(__FILE__) . '/account.php');
    require_once(dirname(__FILE__) . "/SDK/Accounts/etAccounts.class.php");
    require_once(dirname(__FILE__) . "/SDK/Orders/OrderClient.class.php");
  }


}


?>
