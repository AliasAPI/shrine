<?php

/*
 * CONCEPT:
 * Get some Quotes
 * 1. Symbols are provided in an array.
 * 2. Send a request to the API with the symbol array and get back quotes.
 * 3. Format the quotes with the translator() class
 * 4. Return the quotes
 *
 * CAUTION:
 * 2DO: What about missing quotes (or symbols that have been renamed)?
 *
 * $this->response = array(
 * 'alias' => 'testar',
 * 'spirit_time' => '2013-08-01 03:09:00',
 * 'id' => 'DFJF8I0',
 * 'quotes' => array(
 * 'ASTC' => '0.69',
 * 'FMCC' => '1.43',
 * 'FNMA' => '1.54' ) );
 */


// SETUP
$description = "Get the quotes successfully from the broker. Etrade";

require_once('../index.php');

// Instantiate the class with the broker API interface
$broker = new etrade();

$exception = 'no';
$expected_answer = '';
$emergencies = '';

$request['request'] = 'get quotes';
$request['alias'] = 'testar';
$request['broker'] = 'etrade';
$request['get quotes'] = array('GOOG', 'FMCC', 'BADSYM');
$request['spirit_time'] = "2013-06-26 03:06:27";
$request['id'] = 'TRACKIT';

try
{
  // RUN THE TEST
  $response = $broker->run_request( $request );
}
catch( Exception $ex )
{
  print_r($ex);
}


// RESULTS
$emergencies .= ( !$response['quotes']['GOOG'] )
  ? "\$response['quotes']['GOOG'] not returned. <br>" : '';

$emergencies .= ( !$response['quotes']['FMCC']  )
  ? "\$response['quotes']['FMCC'] not returned. <br>" : '';

$emergencies .= ( isset($response['quotes']['BADSYM']  ))
  ? "\$response['quotes']['BADSYM'] returned. <br>" : '';

$emergencies .= ( $response['id'] !== 'TRACKIT' )
  ? "\$response['id'] = TRACKIT not returned. <br>" : '';


?>
