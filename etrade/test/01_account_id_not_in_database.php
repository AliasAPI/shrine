<?php


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'get broker status';
$request['alias'] = 'testar';
$request['broker'] = 'etrade';
$request['spirit_time'] = '2014-12-21 00:56:00';
$request['id'] = time();

// Define the expected shrine response
$response['emergency'] = 'The access_token is missing from tradier.accounts table.';

// Define the extra settings
$settings['description'] = 'The account_id is not in the accounts table.';
$settings['setup_sql'] = "UPDATE `accounts` SET `account_id` = '' WHERE `alias` = '" . $request['alias'] . "' ";

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
