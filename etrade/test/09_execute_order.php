<?php


/*
 * CONCEPT:
 * Place orders
 * 1. Place an order using the request array
 * 2. Get the response
 * 3. Get the ORDERIDs for each lot fill
 * 3. Format the response with the translator.php class
 * 4. Return the formatted response
 *
 * CAUTION:
 */


// SETUP
$description = "Buy a share of two different stocks. Etrade";
$emergencies = '';

require_once('../index.php');

// Instantiate the class with the broker API interface
$broker = new etrade();

$exception = 'no';
$expected_answer = '';

$request['request'] = 'order';
$request['alias'] = 'testar';
$request['spirit_time'] = '2013-08-01 03:09:00';
$request['id'] = 'D' . time();
$request['order'] = array( array( 'symbol' => 'FMCC', 't_type' => 'buy', 'shares' => '1', 'price' => '2.07' ) );
//$request['order'] = array( array( 'symbol' => 'FNMA', 't_type' => 'buy', 'shares' => '1', 'price' => '0.77' ) );


// RUN THE TEST
try
{
  $response = $broker->run_request( $request, $this->db );
}
catch( Exception $ex )
{
  print_r($ex);
  $raised = 'yes';
}


// RESULTS
$emergencies .= ( !$response['successful broker orders'] )
  ? "\$response['successful broker orders'] was not returned. <br>" : '';

?>
