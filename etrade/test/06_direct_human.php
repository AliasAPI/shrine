<?php

/* CONCEPT:
 * First shrine/ must get a token ( $_GET['oauth_verifier'] )
 * The first step is to get a token stored in the database. The
 * shrine system must always first test to make sure there is
 * a token for the account stored in the database. If not . . .
 * Here's how it works:
 * 1. Each user account has an array in shrine/config.php
 * 2. Use the "user" and "pass" to create an Authorization URL
 * 3. If there is no token . . .
 * 3.1 The user should get an email to tell them to browse to http://[shrineserver]/etrade/?alias=user
 * 3.2 The user should be presented with an link to click to authorize the shrine software
 * 3.3 The user will MANUALLY click a link and enter in a different username and password
 * 3.4 The Etrade broker will automatically call http://[shrineserver]/etrade/auth.php (store_token)
 * 3.5 The token will be stored in the database in a row related to the user's account alias
 * 3.6 If the token was not stored (and updated) the user will be redirected to an error page
 * 3.7 If the token was stored correctly (and is current) the user will see a "Thank you!" page
 *
 * CAUTION:
 * Tokens expire every 7 days. Keep in mind that there will be a "renew token" functionality.
 * Make sure this code is designed so that getting a new token and renewing one is similar.
 */



$human_url = "http://" . SHRINE_URL . "/etrade/?alias=testar";

$click_here = "<a href=$human_url target=_blank>CLICK HERE</a> to see the human experience.";

// SETUP
$description = "Direct the human user to the Etrade auth_url.<br><b>$click_here</b>";

// Instantiate the class with the broker API interface
$broker = new etrade();

$exception = 'no';
$expected_answer = '';

// Do not set a $request here

$this->db->Execute("UPDATE `accounts` SET
  `oauth_token` = '',
  `oauth_token_secret` = '',
  `oauth_verifier` = '',
  `access_token` = '',
  `access_token_secret` = '',
  `timestamp` = '0000-00-00 00:00:00',
  `received` = ''
  WHERE `alias` = 'testar' " );


try
{
  // RUN THE TEST
  $returned = file_get_contents('http://' . SHRINE_URL . '/etrade/?alias=testar');
}
catch( Exception $ex )
{
  $raised = 'yes';
}



// RESULTS
// echo "<hr>returned"; echo $returned; die();

list( $results ) = $this->db->GetAll("SELECT * FROM `accounts` WHERE `alias` = 'testar' ");
// print_r( $results ) ; die();

$emergencies .= ( empty( $results['oauth_token'] ) ) ? 'The oauth_token was not recorded.<br>' : NULL;
$emergencies .= ( empty( $results['oauth_token_secret'] ) ) ? 'The oauth_token_secret was not recorded.<br>' : NULL;
$emergencies .= ( strpos( $results['received'], 'human' ) === FALSE ) ? 'The human was not recorded.<br>' : NULL;


?>
