<?php

/*
 * CONCEPT:
 * Get the "broker status"
 * 1. Several attributes of the account are required after each login.
 * 2. All of the info needs to be retrieved, formatted and returned in an array
 *
 * CAUTION:
 */

/*
 * Request format
 * $this->request['request'] = 'get broker status';
 * $this->request['alias'] = 'testar';
 * $this->request['spirit_time'] = '2013-08-01 03:09:00';
 * $this->request['id'] = 'DFJF8I0';
 *
 *
 * Response format
 * $this->response = array(
 * 'alias' => 'drew',
 * 'spirit_time' => '2013-08-01 03:09:00',
 * 'id' => 'DFJF8I0',
 * 'broker status' => array(
 * 'logged_in' => 'yes',
 * 'open_market' => 'yes',
 * 'cash_broker' => '3003',
 * 'broker_positions' => array(
 * array( 'symbol' => 'ASTC', 'shares' => 101 ),
 * array( 'symbol' => 'RHAT', 'shares' => 201 ),
 * array( 'symbol' => 'FMCC', 'shares' => 301 ) ),
 * 'done_orders' => array(
 * array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => 50, 'price' => 1.02,
 * 'date' => '2010-12-06 01:49:00', 'orderid' => 'AKFJD9393'),
 * array( 'symbol' => 'RHAT', 't_type' => 'buy', 'shares' => 201, 'price' => 2.02,
 * 'date' => '2010-12-06 01:49:00', 'orderid' => 'BKFJD9393' ),
 * array( 'symbol' => 'FMCC', 't_type' => 'buy', 'shares' => 301, 'price' => 3.02,
 * 'date' => '2010-12-06 01:49:00', 'orderid' => 'CKFJD9393' ) ),
 * 'open_orders' => array('symbol' => 'ASTC', 'shares' => 51 ),
 * 'logout' => 'yes' ) );
 */


// SETUP
$description = "get_broker_status successfully returns logged_in, cash_broker, open_market. Etrade";

// Instantiate the class with the broker API interface
$broker = new etrade();

$not_ready = '';
$exception = 'no';
$expected_answer = '';


$request['request'] = 'get broker status';
$request['alias'] = 'testar';
$request['broker'] = 'etrade';
$request['spirit_time'] = date('Y-m-d H:i:s');
$request['id'] = mt_rand( 1, 320000 );


list( $acc ) = $this->db->GetAll("SELECT * FROM `accounts` WHERE `alias` = 'testar' ");

$not_ready .= ( empty( $acc['alias'] ) ) ? 'alias, ' : NULL;
$not_ready .= ( empty( $acc['account_id'] ) ) ? 'account_id, ' : NULL;
$not_ready .= ( empty( $acc['oauth_consumer_key'] ) ) ? 'oauth_consumer_key, ' : NULL;
$not_ready .= ( empty( $acc['oauth_consumer_secret'] ) ) ? 'oauth_consumer_secret, ' : NULL;
$not_ready .= ( empty( $acc['oauth_token'] ) ) ? 'oauth_token, ' : NULL;
$not_ready .= ( empty( $acc['oauth_token_secret'] ) ) ? 'oauth_token_secret, ' : NULL;
$not_ready .= ( empty( $acc['oauth_verifier'] ) ) ? 'oauth_verifier, ' : NULL;
$not_ready .= ( $acc['timestamp'] === '0000-00-00 00:00:00' || empty( $acc['timestamp'] ) ) ? 'timestamp, ' : NULL;


// If the accounts table is not ready, halt this test
if( !empty( $not_ready ) )
{
  $emergencies .= "<br><br><center><b>You need to manually authorize shrine with Etrade to run this unit test.</b><br>";
  $human_url = "<a href=http://" . SHRINE_URL . "/etrade/test/?filter=09_ target=_blank>CLICK HERE</a>";
  $emergencies .= "[ $human_url ] and run test 09_direct_human.php.  The [ $not_ready ] values need to be set up. ";
  print $emergencies;
  //die();

}


try
{
  // RUN THE TEST
  $response = $broker->run_request( $request, $this->db );
}
catch( Exception $ex )
{
  $raised = 'yes';
}


// RESULTS
$emergencies .= ( !$response['broker status'] )
  ? "\$response['broker status'] not returned. <br>" : '';

$emergencies .= ( !$response['broker status']['open_market'] )
  ? "\$response['broker status']['open_market'] not returned. <br>" : '';
$emergencies .= ( !$response['broker status']['cash_broker'] )
  ? "\$response['broker status']['cash_broker'] not returned. <br>" : '';
$emergencies .= ( !$response['broker status']['margin'] )
  ? "\$response['broker status']['margin'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['broker_positions'] )
  && $response['broker status']['broker_positions'] !== NULL )
  ? "\$response['broker status']['broker_positions'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['done_orders'] )
  && $response['broker status']['done_orders'] !== NULL )
  ? "\$response['broker status']['done_orders'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['open_orders'] )
  && $response['broker status']['open_orders'] !== NULL )
  ? "\$response['broker status']['open_orders'] not returned. <br>" : '';


// echo("<hr><br>****************REQUEST *************************<br>");
// var_dump($request);
// echo("<br><hr>************RESPONSE *****************************<br>");
// var_dump($response);
// echo("<br>*****************************************<br>");


?>
