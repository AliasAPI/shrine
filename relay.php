<?php


$relay = new relay();


class relay
{
  private $db;
  private $authorized = '';
  private $broker;
  private $recorded = FALSE;
  private $response = array();


  public function __construct()
  {
    $this->__include();

    // Determine if the user has authorizion to send commands
    $this->authorize();

    $this->get_request();

    // If the prophet server is authorized
    if( $this->authorized )
    {
      $this->check_request();

      if( empty( $this->response['emergency'] ) )
      {
        $this->set_database();

        $this->recall();

        if( !$this->recall() )
        {
          $this->record('received');

          $this->set_broker();

          $this->get_response_from_broker();

          $this->record('response');
        }
      }
    }

    // If a human or server browses to this file, hate them.
    if( !$this->request && !$this->authorized )
    {
      echo "I hate you.";
    }
    else
    {
      $this->display_json();
    }
  }



  private function __include()
  {
    $this->response['debug'][] = 'shrine relay()';

    // require_once('config.php');
    // require_once('lib/include_lib.php');

    require_once( dirname(__FILE__) . "/config.php" );
    require_once( dirname(__FILE__) . "/lib/include_lib.php" );

    include_lib();

    // require_once( dirname(__FILE__) . "/lib/db_setup.php" );
    // require_once( dirname(__FILE__) . '/lib/curl.php' );
    // require_once( dirname(__FILE__) . '/lib/json.php' );
  }



  private function authorize()
  {
    // checking $_SERVER['HTTP_AUTHORIZATION'] is set or not
    if( isset( $_SERVER['HTTP_AUTHORIZATION'] ) )
    {
      // extract Basic auth values from  $_SERVER['HTTP_AUTHORIZATION']
      if( preg_match('/Basic\s+(.*)$/i', $_SERVER['HTTP_AUTHORIZATION'], $matches ) )
      {
        list( $name, $password ) = explode(':', base64_decode($matches[1]));
        // Setting
        $from_user = strip_tags( $name );
        $from_pass = strip_tags( $password );

        $this->response['debug'][] = "prophet authorize(): HTTP_AUTHORIZATION is set.";
        $this->response['secret'][] = "from_user = [ " . $from_user . " ]";
        $this->response['secret'][] = "from_pass = [ " . $from_pass . " ]";
        $this->response['secret'][] = "REMOTE_USER = [ " . $_SERVER['REMOTE_USER'] . " ]";
        $this->response['secret'][] = "HTTP_AUTHORIZATION = [ " . $_SERVER['HTTP_AUTHORIZATION'] . " ] ";
      }
    }
    else
    {
      $from_user = $_SERVER['PHP_AUTH_USER'];
      $from_pass = $_SERVER['PHP_AUTH_PW'];
      $this->response['secret'][] = "shrine authorize(): from_user [ " . $from_user . " ] from_pass [ " . $from_pass . " ] ";
    }

    // If spirit sends the right username and password to connect to prophet
    if( $from_user === PROPHET_USER && $from_pass === PROPHET_PASS )
    {
      // Set the username and password that will be sent to the shrines
      $this->response['debug'][] = 'shrine authorize(): prophet authorized';
      $this->authorized = TRUE;
    }
    elseif( $from_user !== PROPHET_USER )
    {
      // If the user authorization fails, send back an error.
      $this->response['emergency'] = 'The prophet user is NOT authorized.';
      $this->response['secret'][] = "shrine authorize(): from_user [ " . $from_user . " ] PROPHET_USER [ " . PROPHET_USER . " ] ";
    }
    elseif( $from_pass !== PROPHET_PASS )
    {
      // If the pass authorization fails, send back an error.
      $this->response['emergency'] = 'The prophet pass is NOT authorized.';
      $this->response['secret'][] = "shrine authorize(): from_pass [ " . $from_pass . " ] PROPHET_PASS [ " . PROPHET_PASS . " ] ";
    }
  }



  private function check_request()
  {
    // Check to make sure all the attributes of the request are valid
    if( empty( $this->request ) )
    {
      $this->response['emergency'] = "The request is missing.";
    }
    elseif( $this->request['alias'] === '' || !$this->request['alias'] )
    {
      $this->response['emergency'] = "The alias is [ " . $this->request['alias'] . " ]";
    }
    elseif( $this->request['broker'] === '' || !$this->request['broker'] )
    {
      $this->response['emergency'] = "The broker is [ " . $this->request['broker'] . " ]";
    }
    elseif( $this->request['spirit_time'] === '' || !$this->request['spirit_time'] )
    {
      $this->response['emergency'] = "The spirit_time is [ " . $this->request['spirit_time'] . " ]";
    }
    elseif( $this->request['id'] === '' || !$this->request['id'] )
    {
      $this->response['emergency'] = "The request id is [ " . $this->request['id'] . " ]";
    }
    elseif( !in_array( $this->request['broker'], array( 'etrade', 'testbroker', 'tradier' ) ) )
    {
      $this->response['emergency'] = "This shrine does not handle the [ " . $this->request['broker'] . " ] broker.";
    }
  }



  private function display_json()
  {
    /*
     * CONCEPT:
     *
     * CAUTION:
     * Do not use <pre> tags to display $this->response.
     * It will cause json_decode() to fail badly.
     */


    // $this->debug[] = "prophet response = [ $response ]";
    if( !$this->response )
    {
      $this->response['warning'][] = "display_json(): Failed to get response";
    }

    if( SHOW_SECRETS !== 'yes' )
    {
      // Remove the sensitive information before echoing
      unset( $this->response['secret'] );
    }

    // Convert the array to JSON
    $response = json_encode( $this->response );

    // Encode characters to UTF8
    $response = utf8_encode( $response );

    // Display the response.
    echo $response;
  }



  private function get_request()
  {
    // Include the class that wraps the CURL library
    $curl = new curl();

    if( !$this->request )
    {
      // Get the posted array and any debugging messages
      list( $this->request, $curl_debug ) = $curl->pull_post();

      if( is_array( $curl_debug ) && SHOW_SECRETS === 'yes' )
      {
        $this->response['secret'][] = "get_request() [ " . print_r( $curl_debug, TRUE ) . " ]";
      }
    }

    // Add the some of the request attributes to the response
    $this->response['alias'] = $this->request['alias'];
    $this->response['spirit_time'] = $this->request['spirit_time'];
    $this->response['id'] = $this->request['id'];

    // Sort the array so that it is a tad more human readable
    ksort( $this->response );
  }



  private function get_response_from_broker()
  {
    // var_dump( $this->response ); die();
    if( $this->recorded && !$this->response['emergency'] && isset( $this->broker ) )
    {
      $this->response['debug'][] = "get_response_from_broker()";

      // Use $this->broker is set in $this->set_broker()
      $this->response = $this->broker->run_request( $this->request, $this->response, $this->db );

      // Sort the array so that it is a tad more human readable
      ksort( $this->response );

      if( defined('SHOW_SECRETS') && SHOW_SECRETS === 'yes' )
      {
        $this->response['debug'][] = "get_response_from_broker() secrets allowed";

        // Put the secret output in the chronological order of the debug array
        $this->response['debug']['secret'] = $this->response['secret'];

        // Unset the secret so the output will not be duplicated
        unset( $this->response['secret'] );
      }
      else
      {
        unset( $this->response['secret'] );
      }
    }
  }



  private function recall()
  {
    // Instantiate the class that handles the JSON
    $json = new json();

    $recalled = FALSE;

    // Pull the account from the database
    list( $db_account ) = $this->db->GetAll("SELECT * FROM `accounts`
      WHERE `alias` = '" . $this->request['alias'] . "' LIMIT 1 " );

    // $this->response['debug'][] =  "check_account() = " . print_r( $db_account, TRUE ) . " ";

    $received_request_json = $db_account['received'];
    $received_request = $json->to_array( $received_request_json );

    // If the request has already been received (and processed)
    if( !empty( $db_account['received'] ) && $this->request['id'] === $received_request['id'] )
    {
      $recalled = TRUE;
      // Unset the response since it is being replaced with response previously received from the broker
      $this->response = array();

      $this->response = $json->to_array( $db_account['response'] );
      $this->response['warning'][] = "Duplicate request received. Returning the recorded response.";
      // var_dump( $this->response ); die();
    }

    return $recalled;
  }



  private function record( $from )
  {
    /*
     * CONCEPT:
     *
     * CAUTION:
     */


    // Instantiate the class that handles the JSON
    $json = new json();

    // Define which tranmission should be recorded
    // received = The transmission that comes from the prophet server
    // response = The transmission response from the broker
    $record_array = ( $from === 'received' ) ? $this->request : $this->response;

    // Convert the array to JSON for storage
    $record_json = $json->encode( $record_array );

    // Record the request in from_spirit
    $this->db->Execute("UPDATE `accounts` SET `" . $from . "` = '" . $record_json . "'
      WHERE `alias` = '" . $this->request['alias'] . "' ");

    // Get the stored request
    $stored_request = $this->db->Execute("SELECT `" . $from . "` FROM `accounts`
      WHERE `alias` = '" . $this->request['alias'] . "' ");

    if( $record_json === $stored_request->fields[$from] )
    {
      // Set a flag so get_response_from_broker() will proceed
      $this->recorded = TRUE;
    }

    $this->response['debug'][] = "[ $from ] recorded.";
  }



  private function set_broker()
  {
    // Include the code for the correct broker. Each broker has it's own folder
    require_once( DB_NAME . '/index.php' );

    // Select the broker
    if( DB_NAME === 'etrade' )
    {
      // Instantiate the broker class
      $this->broker = new etrade();
    }
    elseif( DB_NAME === 'testbroker' )
    {
      // Instantiate the broker class
      $this->broker = new testbroker();
    }
    elseif( DB_NAME === 'tradier' )
    {
      // Instantiate the broker class
      $this->broker = new tradier();
    }
  }



  private function set_database()
  {
    // Set the database name that will be used for the entire session
    define('DB_NAME', $this->request['broker'] );

    // Connect to the database and setup a database for the broker
    $db_setup = new db_setup();

    $db_setup->build( $this->request );

    // var_dump( $db_setup ); die();

    // Create the database object that gets passed around
    $this->db = $db_setup->db;

    foreach( $db_setup->debug AS $i => $db_message )
    {
      $this->response['debug'][] = $db_message;
    }

    if( $db_setup->emergency )
    {
      $this->response['emergency'] = $db_setup->emergency;
    }

    if( !empty( $db_setup->warning ) )
    {
      $this->response['warning'][] = $db_setup->warning;
    }
    // var_dump( $this->response ); die();
  }


}


?>
