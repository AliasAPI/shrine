<?php


class quotes
{
  private $request;
  private $response;


  public function get_quotes( $request )
  {

    $this->__include();

    $json = new json();

    $utils = new utils();

    $quotes = array();
    $symbols = "";

    foreach( $request['get quotes'] AS $symbol => $null )
    {
      $symbols = $symbols . "," . $symbol;
    }

    $symbols = substr( $symbols, 1 );
    $params = array( 'symbols' => $symbols );

    list( $broker_response, $debug ) = $utils->jam( 'GET', 'quotes', $params );
    // echo "<pre>"; print_r( $debug ); die();

    $this->response['secret']['get_quotes'] = $debug;

    if( $broker_response )
    {
      foreach( $broker_response['quotes']['quote'] AS $i => $quote )
      {
        // Ignore the OTC market . . .
        if( ( $quote['exch'] !== 'U' && $quote['exch'] !== 'V' )
          // . . . unless the symbols are FMCC or FNMA
          || ( $quote['symbol'] == 'FMCC' || $quote['symbol'] == 'FNMA' ) )
        {
          $res[$quote['symbol']] = number_format( $quote['last'], 2, '.', '' );
        }
      }

      $this->response['quotes'] = $res;
    }
    else
    {
      $this->response['warning'] = 'No response returned in quotes.';
    }

    return $this->response;
  }



  private function __include()
  {
    // Include the class that contains the JSON library
    require_once( dirname(__FILE__) . "/../lib/json.php" );

    // Include the class that has tradier utilities
    require_once( dirname(__FILE__) . "/utils.php" );

    // Include a list of the usernames passes and respective brokers
    // include( dirname(__FILE__) . "/config.php");
  }




}


?>
