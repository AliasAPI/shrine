<?php


class order
{
  private $request;
  private $response;


  public function execute( $request )
  {
    /*
     * CONCEPT:
     * process_trades() executes orders in the brokerage account.
     * EXAMPLE order:
     * $request['order'] = array( array( 'symbol' => 'FMCC',
     * 't_type' => 'buy', 'shares' => '1', 'price' => '0.77',
     * 'order_type' => 'market' ) );
     *
     * CAUTION:
     * Only return successfull orders
     * Check the status of the order: If 'expired' or 'rejected' ?
     * status	Status of the order (filled, canceled, open, expired,
     * rejected, pending, partially_filled, submitted)
     */

    $this->request = array();
    $this->request = $request;
    $this->response = array();
    $res = array();

    $this->__include();

    $utils = new utils();

    // Count the number of orders in the order array.
    $count = count( $request['order'] );

    // see:order_placement_loop
    foreach( $request['order'] AS $index => $ord )
    {
      // Tally the orders to keep a record of the orders posted
      $number++;

      // i:dupe_stop
      if( $number <= $count )
      {
        // echo "Order<pre> " .  print_r( $ord, TRUE ) . "</pre><br>";

        // $params['account'] = ACCOUNT_ID;
        $params['class'] = 'equity';
        // i:append_attributes
        $params['symbol'] = $ord['symbol'];
        $params['duration'] = 'day'; // Change???
        $params['side'] = $ord['t_type'];
        $params['quantity'] = $ord['shares'];
        $params['type'] = ( isset( $ord['order_type'] ) ) ? $ord['order_type'] : 'market';
        $params['price'] = $ord['price'];
        $params['preview'] = ( $ord['preview'] === TRUE ) ? TRUE : FALSE;
        // $params['preview'] = TRUE; // 2DO+++ remove this to stop blocking orders

        // $params['stop'] = NULL; Only required for "stop" and "stop_limit"
        // $params['option_symbol'] = An OCC formatted option symbol. Example: AAPL140118C00195000 Required for "option" orders
        // var_dump( $params ); die();

        // Get the Broker Response and the debug
        list( $broker_response, $debug ) = $utils->jam( 'POST', 'orders', $params );

        $this->response['secret']['order'] = $debug;

        if( is_array( $broker_response ) && !empty( $broker_response ) && $broker_response['order']['status'] === 'ok' )
        {
          $res['symbol'] = ( $ord['preview'] === TRUE ) ? strval( $broker_response['order']['symbol'] ) : $ord['symbol'];
          $res['t_type'] = ( $ord['preview'] === TRUE ) ? strval( $broker_response['order']['side'] ) : $ord['t_type'];
          $res['shares'] = ( $ord['preview'] === TRUE ) ? strval( $broker_response['order']['quantity'] ) : $ord['shares'];
          $res['status'] = strval( $broker_response['order']['status'] );

          // If the order is a preview, return the preview responses . . .
          if( $ord['preview'] === TRUE )
          {
            $res['commission'] = strval( $broker_response['order']['commission'] );
            $res['cost'] = strval( $broker_response['order']['cost'] );
            $res['extended_hours'] = strval( $broker_response['order']['extended_hours'] );
            $res['fees'] = strval( $broker_response['order']['fees'] );
            $res['margin_change'] = strval( $broker_response['order']['margin_change'] );
            $res['fees'] = strval( $broker_response['order']['fees'] );
          }
          else
          {
            // . . . otherwise return the confirmation (orderid) number
            $res['orderid'] = strval( $broker_response['order']['id'] );
          }

          // i:dupe_debug
          $res['count'] = "order [ $number ] of [ $count ] ";

          // Put all the results in a response array
          $this->response['successful broker orders'][] = $res;
        }
      }
      // echo "<b>FINAL RESPONSE<br>in order.php</b><br><pre>" . print_r( $this->response, TRUE ) . "</pre><hr>";
    }
    // var_dump( $this->response ); die();

    return $this->response;
  }



  private function __include()
  {
    // Include the class that contains the JSON library
    require_once( dirname(__FILE__) . "/../lib/json.php" );

    // Include the class that has tradier utilities
    require_once( dirname(__FILE__) . "/utils.php" );

    // Include a list of the usernames passes and respective brokers
    // include( dirname(__FILE__) . "/config.php");
  }


}


?>
