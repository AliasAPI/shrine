<?php


/*
 * CONCEPT:
 * Place orders
 * 1. Place an order using the request array
 * 2. Get the response
 * 3. Get the ORDERIDs for each lot fill
 * 3. Format the response with the translator.php class
 * 4. Return the formatted response
 *
 * CAUTION:
 */

// $request['order'][] = array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => '3', 'price' => '2.76' ) ;
// $request['order'][] = array( 'symbol' => 'CRNT', 't_type' => 'buy', 'shares' => '3', 'price' => '1.25' ) ;

// $request['order'][] = array( 'symbol' => 'QCOM', 't_type' => 'buy', 'shares' => '10', 'price' => '79.52' ) ;
// $request['order'][] = array( 'symbol' => 'CAKE', 't_type' => 'buy', 'shares' => '10', 'price' => '44.85' ) ;
// echo "<hr>REQUEST:<pre>" . print_r( $request, TRUE ) . "</pre><hr>";

// RESULTS
// $emergencies .= ( !$response['successful broker orders'] )
//  ? "\$response['successful broker orders'] was not returned. <br>" : '';


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'order';
$request['alias'] = 'testar';
$request['broker'] = 'tradier';
$request['spirit_time'] = date('Y-m-d H:i:s', time() );
$request['id'] = time();

$request['order'][] = array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => '1',
  'price' => '3.00', 'order_type' => 'market', 'preview' => TRUE ) ;
$request['order'][] = array( 'symbol' => 'DDE', 't_type' => 'buy', 'shares' => '2',
  'price' => '1.13', 'order_type' => 'limit', 'preview' => TRUE ) ;

// Define the expected shrine response
// $response[''] = '';

// Define the extra settings
$settings['description'] = 'Buy a share of two different stocks.';

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
