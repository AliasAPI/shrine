<?php


require_once( dirname(__FILE__) . "/../../config.php" );

echo "<br><center><table><tr><td><b>Tradier Unit Tests</b><br><br>";

// Create the path to the test files dynamically
$directory = dirname(__FILE__) . DIRECTORY_SEPARATOR;

// Load all of the unit test files into an array
$phpfiles = glob( $directory . '*' );

foreach( $phpfiles AS $directory_file )
{
  if( strpos( $directory_file, 'php' ) !== FALSE && !strpos( $directory_file, 'menu' ) )
  {
    // Clean up the test name
    $file = str_replace("$directory", '', $directory_file );

    if( $file !== '' )
    {
      echo "<a href=http://" . SHRINE_URL . "/tradier/test/$file >$file</a><br><br>";
    }
  }
}

echo "</td></tr></table>";


?>
