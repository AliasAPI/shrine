<?php


/*
// RESULTS
$emergencies .= ( !$response['broker status'] )
  ? "\$response['broker status'] not returned. <br>" : '';

$emergencies .= ( !$response['broker status']['open_market'] )
  ? "\$response['broker status']['open_market'] not returned. <br>" : '';
$emergencies .= ( !$response['broker status']['cash_broker'] )
  ? "\$response['broker status']['cash_broker'] not returned. <br>" : '';
$emergencies .= ( !$response['broker status']['margin'] )
  ? "\$response['broker status']['margin'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['broker_positions'] )
  && $response['broker status']['broker_positions'] !== NULL )
  ? "\$response['broker status']['broker_positions'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['done_orders'] )
  && $response['broker status']['done_orders'] !== NULL )
  ? "\$response['broker status']['done_orders'] not returned. <br>" : '';

$emergencies .= ( !is_array( $response['broker status']['open_orders'] )
  && $response['broker status']['open_orders'] !== NULL )
  ? "\$response['broker status']['open_orders'] not returned. <br>" : '';
*/


// Include the test file
require_once( dirname(__FILE__) . "/../../lib/test.php");

// Define the prophet request
$request['request'] = 'get broker status';
$request['alias'] = 'testar';
$request['broker'] = 'tradier';
$request['spirit_time'] = date( 'Y-m-d H:i:s', time() );
$request['id'] = mt_rand( 1, 350000 );

// Define the expected shrine response
// $response[''] = '';

// Define the extra settings
$settings['description'] = 'get_broker_status successfully returns logged_in, cash_broker, open_market.';

// RUN THE TEST
$test = new test( $request, $response, $settings );


?>
