

ISSUE: 'cash_available' is inconsistent
What if there is a margin call and the account falls back to cash?




ISSUE: Duplicate orders
Unlike other brokers, Tradier's API cannot tell whether or not an order is a duplicate. 
Before trading, I ran numerous tests against Tradier's API.  However, on the first day,
Tradier claims that shrine sent duplicate orders. It is a problem I have been unable to 
duplicate through testing.
FIXES:
Unit test 06_ignore_duplicates.php proves that shrine can detect duplicate orders using
the tracking id sent with all requests. The code for this is found at i:dupe_warning in
/tradier/accounts.php .  If there are any warnings, shrine will not send the request to
the Tradier API.  Also, debugging was added to prove that shrine only loops through the
order array once (at i:dupe_debugging).  Finally, there is extra code to make sure that
the order array is only processed once at i:dupe_stop . This issue was also tested from
upstream servers in the order process as well.  There is no additional code that can be
added in shrine to prevent duplicate orders.
SOLUTION:
Tradier's order API should be upgraded to recognize duplicate orders in this way:
1. Add a parameter for placing orders called 'client_id'
2. Allow the client_id to be 20 characters. I'd like to use "symbol 5 digit tracking_id"
3. Allow the client_id to be optional. Those that use client_id prevent duplicate orders
4. Ignore any order that uses a client_id that has already been received for the account.
5. Return the  symbol, quantity, side, timestamp, client_id, status=OK, order confirm id
6. Do NOT return any information related to any orders that were NOT placed at that time
7. Always return the client_id with Tradier's current order 'id'. It is the same concept.



ISSUE:
On 11/03/14 Tradier's API to placed duplicate orders (and overspent a *cash* account)!
Apparently, the developers use, in part, the datetime stamp (in seconds) to determine
whether or not there is cash in the account.
FIXES:
Shrine now pauses for 1 second between each order (at i:retard_orders) so Tradier's API
will catch up to the math.  There is no additional code that can be added to shrine to
help Tradier's API. 



ISSUE:
When Tradier's API sends back a response to a successful order placement, it only
includes 'status=OK' and an order id. Rather than send back the attributes of the
successful order, Tradier's developers want everyone to look up the status of the
order using the order id that was returned.
FIXES:
For now, code at i:append_attributes has been added to fill in the missing order
information. Later, when Tradier's order API returns the attributes, we will fix
the code so that it returns the response (rather than the request) values.



ISSUE:
The Tradier API only sent back ONE order id when the 6 orders were placed. Like
the above issues, this malfunction was a one time occurance.
FIXES:
No code modifications can be made. Ran more unit tests and everything worked as
expected.



ISSUE:
At around 8PM on 11/03/14, Tradier's API stopped returning the order fill info.
That means shrine is unable to pull the order status (especially without the id
of each successful order). Without the order fill info the entire system fails. 
FIXES:
No code can be added. The order API is simply not returning the order fill info.
My current guess is that the order information is returned for the day up until
8PM EST.
