<?php


class status
{
  private $request;
  private $response;


  public function get_status( $request )
  {
    /*
     * CONCEPT:
     * shrine/status.php retrieves the current status information from the broker.
     * Some brokers (like just2trade) automatically send back all of the information
     * immediately. If that happens, the attributes are sent to other functions to be
     * split up and returned to jesus. Otherwise, the functions pull the information.
     * The entire class is meant to compile a 'response' array to be sent to shrine
     *
     * CAUTION:
     * Use minimal error checking and data validation. jesus should validate the data.
     *
     *
     * REQUEST:
     * $request = array(
     *  'request' => 'get broker status',
     *  'account' => 'testar',
     *  'spirit_time' => '2013-10-01 10:10:00',
     *  'id' => 'TRACKIT' );
     *
     * RESPONSE:
     * $this->response = array(
     *  'account' => 'testar',
     *  'spirit_time' => '2013-08-01 03:09:00',
     *  'id' => 'TRACKIT',
     *  'broker status' => array(
     *  'open_market' => 'yes',
     *  'cash_broker' => '3003',
     *  'margin' => 'yes',
     *  'broker_positions' => array(
     *    array( 'symbol' => 'ASTC', 'shares' => 101 ),
     *    array( 'symbol' => 'RHAT', 'shares' => 201 ),
     *    array( 'symbol' => 'FMCC', 'shares' => 301 ) ),
     *  'done_orders' => array(
     *    array( 'symbol' => 'ASTC', 't_type' => 'buy', 'shares' => 50, 'price' => 1.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'AKFJD9393'),
     *    array( 'symbol' => 'RHAT', 't_type' => 'buy', 'shares' => 201, 'price' => 2.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'BKFJD9393' ),
     *    array( 'symbol' => 'FMCC', 't_type' => 'buy', 'shares' => 301, 'price' => 3.02,
     *    'date' => '2010-12-06 01:49:00', 'orderid' => 'CKFJD9393' ) ),
     *  'open_orders' => array('symbol' => 'ASTC', 'shares' => 51 ) ) );
     */

    $this->request = array();
    $this->request = $request;
    $this->response = array();

    $this->includes();

    // Check to see if the account allows margin (for Regulation T and Rule 431)
    $this->get_margin();

    // Get the current cash balance
    $this->get_cash_broker();

    // Reorder the output to put the account_value first
    ksort( $this->response['broker status'] );

    // Check to see if the stock markets are open
    $this->check_markets();

    // Get the broker_positions to get the number of shares owned for each symbol
    $this->get_broker_positions();

    // Get the new done_orders
    $this->get_done_and_open_orders();

    // var_dump( $this->response ); die();
    // $response = $this->response;
    if( SHOW_SECRETS !== 'yes' )
    {
      unset( $this->response['debug'] );
      unset( $this->response['secret'] );
    }

    return $this->response;
  }



  private function check_markets()
  {
    /*
     * CONCEPT:
     * check_markets() checks to see if the markets are currently open.
     * If the market is open, return 'yes', otherwise return 'no'
     *
     * /v1/markets/clock
     * <state>open</state>
     *
     * CAUTION:
     */


    $utils = new utils();

    list( $broker_response, $debug ) = $utils->jam( 'GET', 'clock', $params );

    $this->response['secret']['check_markets'] = $debug;

    $this->response['broker status']['open_market'] = ( $broker_response['clock']['state'] === 'open' ) ? 'yes' : 'no';
  }



  private function clean_date( $date )
  {
    // Example: <transaction_date>2014-05-28T12:04:52.627Z</transaction_date>

    // Replace the T with a space
    $date = str_replace('T', ' ', $date );

    // Remove the last 5 characters
    $date = substr( $date, 0, -5 );

    // Subtract 4 hours from the time
    $date = date('Y-m-d H:i:s', strtotime( $date ) - 60 * 60 * 4 );

    return $date;
  }



  private function fill( $order )
  {
    /*
     * CONCEPT:
     * status->fill() ignores the extra order fill information (for orders that were not executed).
     * For executed orders, fill() builds an 'done' and 'open' multi-demensional array.
     *
     * CAUTION:
     */


    // If the order is not one of Tradier's extra or rejected orders
    if( $order['exec_quantity'] == 0 )
    {
      // Disregard orders that do not have executed shares
      echo '';
    }
    else
    {
      if( is_array( $order ) && !empty( $order ) )
      {
        // Clean up the date
        $datetime = $this->clean_date( $order['transaction_date'] );



        // Return an array like this:
        // $done_orders[] = array('symbol' => "$symbol", 't_type' => "$t_type", 'shares' => "$shares",
        // 'price' => "$price", 'date' => "$t_date", 'orderid' => "$orderid" );

        $this->response['broker status']['done_orders'][] = array(
          'symbol' => "" . $order['symbol'] ."",
          't_type' => "" . $order['side'] . "",
          'shares' => "" . $order['exec_quantity'] . "",
          'price' => "" . $order['avg_fill_price'] . "",
          'date' => "" . $datetime . "",
          'orderid' => "" . $order['id'] . "" );

        // Here, rather than using get_open_orders(), just process the orders.
        // Get the number of due_shares for each incomplete transaction.
        // The array format:
        // $open_orders[] = array('symbol' => "$symbol", 't_type' => "side", 'shares' => "$open_shares" ));

        if( $order['remaining_quantity'] > 0 )
        {
          $this->response['broker status']['open_orders'][] = array(
            'symbol' => "" . $order['symbol'] ."",
            't_type' => "" . $order['side'] . "",
            'shares' => "" . $order['remaining_quantity'] . "" );
        }
      }
    }
  }



  private function get_broker_positions()
  {
    /*
     * CONCEPT:
     * get_broker_positions() all of the symbols and the number of shares for each stock owned in the brokerage account.
     * The array format: $broker_positions[] = array("$symbol" => array('shares' => "$shares"));
     *
     * /v1/user/positions
     * quantity	Number of shares held.
     * symbol	Symbol
     *
     * CAUTION:
     * It's important to count all of the symbols currently owned so that sacrifice() will work correctly.
     */


    $utils = new utils();

    list( $broker_response, $debug ) = $utils->jam( 'GET', 'positions', $params );

    $this->response['secret']['get_broker_positions'] = $debug;

    if( is_array( $broker_response ) && !empty( $broker_response ) )
    {
      // [accounts] => Array
      foreach( $broker_response['accounts'] AS $i1 => $account )
      {
        // [account] => Array
        // echo "<hr>accounts<br>"; var_dump($broker_response['accounts'] );
        if( is_array( $account ) && !empty( $account ) )
        {
          // [positions] => Array
          foreach( $account AS $i2 => $positions )
          {
            // echo "<hr>positions<br>"; var_dump( $positions );
            if( is_array( $positions ) && !empty( $positions ) )
            {
              // [position] => Array
              foreach( $positions AS $i3 => $position )
              {
                if( is_array( $position ) && !empty( $position ) )
                {
                  // If there is only one position, Tradier does not nest it?
                  if( isset( $position['symbol'] ) )
                  {
                    $this->response['broker status']['broker_positions'][] =
                      array('symbol' => "" . $position['symbol'] . "", 'shares' => "" . floor( $position['quantity'] ) . "" );
                  }
                  else
                  {
                    foreach( $position AS $i4 => $pos )
                    {
                      if( is_array( $pos ) && !empty( $pos ) )
                      {
                        // [cost_basis] => 618.2044
                        // [date_acquired] => 2014-11-03T19:49:20.553Z
                        // [id] => 3360
                        // [quantity] => 1199
                        // [symbol] => MSTX

                        $this->response['broker status']['broker_positions'][] =
                          array('symbol' => "" . $pos['symbol'] . "", 'shares' => "" . floor( $pos['quantity'] ) . "" );
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    if( !is_array( $this->response['broker status']['broker_positions'] ) )
    {
      $this->response['broker status']['broker_positions'] = NULL;
    }
  }



  private function get_cash_broker()
  {
    /*
     * CONCEPT:
     * get_cash_broker() returns $S->cash_local for use as $S->cash_broker.
     * Note: This is only used when using the various testing modes.  Don't
     * round the number here. That will be handeled in upstream code.
     *
     * /v1/accounts/{account_id}/balances
     * cash_available
     * The amount of cash that could be withdrawn or invested in new investments,
     * cash that is not required to support existing positions
     *
     * CAUTION:
     * Make sure to match the account number in the returned array.
     */


    $utils = new utils();

    list( $broker_response, $debug ) = $utils->jam( 'GET', 'cash_available', $params );
    // var_dump( $broker_response ); die();
    // var_dump( $debug );

    $this->response['secret']['get_cash_broker'] = $debug;

    // Important: Run get_margin() before this method to determine the value to use
    if( $this->response['broker status']['margin'] === 'no' )
    {
      // If the brokerage account is a cash account . . .
      $this->response['broker status']['cash_broker'] = ( $broker_response['balances']['cash']['cash_available'] == '0' ) ? '0.00' : strval( number_format( $broker_response['balances']['cash']['cash_available'], 2, '.', '' ) );
    }
    else
    {
      // Tradier support said: "total_cash" - "option_requirement" - "pending_cash" = "cash_available"
      $cash_available = $broker_response['balances']['total_cash'] - $broker_response['balances']['option_requirement'] - $broker_response['balances']['pending_cash'];

      // If the account is a margin account . . .
      $this->response['broker status']['cash_broker'] = ( $cash_available == '0' ) ? '0.00' : strval( number_format( $cash_available, 2, '.', '' ) );
    }

    // Show the account value according to the broker
    $this->response['broker status']['account_total'] = ( $broker_response['balances']['total_equity'] == '0' ) ? '0.00' : strval( number_format( $broker_response['balances']['total_equity'], 2, '.', '' ) );
  }



  private function get_done_and_open_orders()
  {
    /*
     * CONCEPT:
     * shrine->get_done_and_open_orders() gets the order fills for each symbol.
     * For example, if an order wants to buy 5000 shares of a symbol, there may
     * be several order fills for smaller amounts of shares at different prices
     * until the order is completely filled. get_done_and_open_orders() fetches
     * the smaller order fills so that they can be recorded.
     *
     * CAUTION:
     */

    $utils = new utils();

    list( $broker_response, $debug ) = $utils->jam( 'GET', 'orders', $params );

    $this->response['secret']['get_done_and_open_orders'] = $debug;

    // echo "<hr>broker_response get_done_and_open_orders()<pre>"; print_r( $broker_response );
    // echo "<br>debug<br>"; var_dump( $debug ); echo "</pre>"; die();

    if( is_array( $broker_response ) && !empty( $broker_response ) )
    {

      // When Tradier returns more than one order fill, they add an extra dimension
      foreach( $broker_response AS $i_1 => $level_1 )
      {
        if( is_array( $level_1 ) && !empty( $level_1 ) )
        {
          foreach( $level_1 AS $i_2 => $level_2 )
          {
            // echo "<hr>orders_container<pre>"; print_r( $orders_container ); echo "</pre>"; die();
            if( is_array( $level_2 ) && !empty( $level_2 ) )
            {
              // echo "<hr>order<pre>"; print_r( $order_container ); echo "</pre>"; die();
              foreach( $level_2 AS $l_3 => $order )
              {
                $this->fill( $order );
              }
            }
          }
        }
      }


      // When Tradier returns a SINGLE order fill, they only use two dimensions
      foreach( $broker_response AS $i_1 => $level_1 )
      {
        if( is_array( $level_1 ) && !empty( $level_1 ) )
        {
          foreach( $level_1 AS $i_2 => $order )
          {
            $this->fill( $order );
          }
        }
      }

    }


    // If there are no done_orders return the placeholder
    if( !is_array( $this->response['broker status']['done_orders'] ) )
    {
      $this->response['broker status']['done_orders'] = NULL;
    }

    // If there are no open_orders return the placeholder
    if( !is_array( $this->response['broker status']['open_orders'] ) )
    {
      $this->response['broker status']['open_orders'] = NULL;
    }

    // echo "<hr>broker status in get_done_and_open_orders()<pre>"; print_r( $this->response ); echo "</pre>"; die();
  }



  private function get_margin()
  {
    /*
     * CONCEPT:
     * If the user has a margin account, return 'yes' . . . otherwise return 'no'
     *
     * /v1/user/profile
     * type	Account Type (cash or margin)
     *
     * <profile><account><type>
     *
     * CAUTION:
     * get_margin() MUST BE RUN before get_cash_broker() .
     */

    $utils = new utils();

    list( $broker_response, $debug ) = $utils->jam( 'GET', 'profile', $params );

    $this->response['secret']['get_margin'] = $debug;

    if( $broker_response['profile']['account']['type'] === 'margin' )
    {
      $this->response['broker status']['margin'] = 'yes';
    }
    elseif( $broker_response['profile']['account']['type'] === 'cash' )
    {
      $this->response['broker status']['margin'] = 'no';
    }
    else
    {
      $this->response['broker status']['margin'] = $broker_response['profile']['account']['type'];
    }

    // var_dump( $broker_response ); echo $broker_response['profile']['account']['type']; die();
  }



  private function includes()
  {
    // Include the class that contains the JSON library
    require_once( dirname(__FILE__) . "/../lib/json.php" );

    // Include the class that has tradier utilities
    require_once( dirname(__FILE__) . "/utils.php" );

    // Include a list of the usernames passes and respective brokers
    // include( dirname(__FILE__) . "/config.php");
  }


}

/*
      // If there are multiple order fills
      if( is_array( $broker_response['orders']['order'] ) && !empty( $broker_response['orders']['order'] ) )
      {

      }
      elseif( is_array( $broker_response['orders']['order'] ) && !empty( $broker_response['orders']['order'] ) )
      {

      }
 */

?>
