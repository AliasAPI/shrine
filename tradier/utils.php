<?php


// $utils = new utils( 'GET', 'clock', '' );
// list( $br, $debug ) = $utils->jam( 'GET', 'clock', '' ); var_dump( $utils );

class utils
{
  public $response = array();
  private $secret = array();
  public $debug = array();
  private $method;
  private $params = array();
  public $warning;


  public function jam( $method, $action, $params=NULL )
  {
    $this->__include( $method, $action, $params );

    $request_url = $this->build_request_url();

    if( $this->method === 'GET' )
    {
      $response_json = $this->transmit_get( $request_url );
    }
    elseif( $this->method === 'POST' )
    {
      $response_json = $this->transmit_post( $request_url );
    }
    else
    {
      $this->debug[] = "jam() ERROR: this->method = [ $this->method ] ";
    }

    $this->response = $this->response_array( $response_json );

    return array( $this->response, $this->debug );
  }



  private function __include( $method, $action, $params )
  {
    $this->action = $action;
    $this->method = $method;
    $this->response = array();

    if( is_array( $params ) && !empty( $params ) )
    {
      $this->params = $params;
    }
    else
    {
      $this->params = array();
    }

    if( !defined( 'DB_NAME' ) )
    {
      define('DB_NAME', 'tradier' );
    }

    // Include a list of the usernames passes and respective brokers
    require_once( dirname(__FILE__) . "/../config.php");

    // Include the class that contains the JSON library
    require_once( dirname(__FILE__) . "/../lib/json.php" );

    $this->debug[] = "utils->jam() method = [ $this->method ] action = [ $this->action ], params = [ $this->params ] ";
  }



  function get_endpoint()
  {
    $endpoint = 'https://api.tradier.com';

    if( $this->action === 'cash_available' )
    {
      // var_dump( $account ); die();
      $endpoint .= "/v1/accounts/" . ACCOUNT_ID . "/balances";
    }
    elseif( $this->action === 'clock' )
    {
      // var_dump( $account ); die();
      $endpoint .= '/v1/markets/clock';
    }
    elseif( $this->action === 'orders' )
    {
      // var_dump( $account ); die();
      $endpoint .= "/v1/accounts/" . ACCOUNT_ID . "/orders";
    }
    elseif( $this->action === 'positions' )
    {
      // var_dump( $account ); die();
      $endpoint .= '/v1/user/positions';
    }
    elseif( $this->action === 'profile' )
    {
      // var_dump( $account ); die();
      $endpoint .= '/v1/user/profile';
    }
    elseif( $this->action === 'quotes' )
    {
      // var_dump( $account ); die();
      $endpoint .= '/v1/markets/quotes';
    }

    $this->debug[] = "get_endpoint() = [ $endpoint ] ";

    return $endpoint;
  }



  function build_request_url()
  {
    $this->debug[] = "build_request_url() action [ $this->action ] params [ $this->params ]";

    //var_dump( $this->debug ); die();
    // Get the Tradier "endpoint" (which is the URL to send the request to)
    $endpoint = $this->get_endpoint();

    if( $this->params !== NULL && $this->method !== 'POST' )
    {
      $request_url = $endpoint . '?' . http_build_query( $this->params );
    }
    else
    {
      $request_url = $endpoint;
    }

    $this->debug[] = "build_request_url() = [ $request_url ]";

    return $request_url;
  }



  function transmit_get( $request_url )
  {
    if( $this->method === 'GET' )
    {
      $this->debug[] = "transmit_get() request_url = [ $request_url ] ";

      // if( $this->action = 'order' ) { $request_url = $request_url . '/15279'; }

      $ch = curl_init( $request_url );

      $header_array = array('Accept: application/json', "Authorization: Bearer " . ACCESS_TOKEN . "");
      // $this->debug[] = "transmit_get() header_array = [ " . print_r( $header_array, TRUE ) . " ] ";

      curl_setopt( $ch, CURLOPT_HTTPHEADER, $header_array );
      curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

      $response_json = curl_exec( $ch );

      // Pull the http status
      $http_status = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

      curl_close( $ch );

      $this->debug[] = "transmit_get() http status = [ " . $http_status . " ]";
     // $this->debug[] = "transmit_get() response_json = [ " . print_r( $response_json, TRUE ) . " ]";
    }

    return $response_json;
  }



  function transmit_post( $request_url )
  {
    if( $this->method === 'POST' )
    {
      $this->debug[] = "transmit_post() request_url = [ $request_url ] ";

      $ch = curl_init( $request_url );

      $header_array = array('Accept: application/json', "Authorization: Bearer " . ACCESS_TOKEN . "");
      // $this->debug[] = "transmit_post() header_array = [ " . print_r( $header_array, TRUE ) . " ] ";

      curl_setopt( $ch, CURLOPT_HTTPHEADER, $header_array );
      curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
      // curl_setopt($ch, CURLOPT_POST, count($params));
      curl_setopt( $ch, CURLOPT_POSTFIELDS, $this->params );

      $response_json = curl_exec( $ch );

      curl_close( $ch );

      // $this->debug[] = "transmit_post() response_json = [ " . print_r( $response_json, TRUE ) . " ]";
    }

    return $response_json;
  }



  function response_array( $response_json )
  {
    $json = new json();

    $response_array = $json->to_array( $response_json );

    if( is_array( $response_array ) && !empty( $response_array ) )
    {
      foreach( $response_array AS $i => $res )
      {
        // $this->debug[] = "response_array() = [ " . print_r( $response_array, TRUE ) . " ]";
        $this->debug['response_array'] = $res;
      }
    }
    else
    {
      $this->warning[] = "The \$response_json [ $response_json ] is not valid JSON.";
    }

    return $response_array;
  }


}


?>
