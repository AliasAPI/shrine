<?php


class account
{
  private $alias;
  private $access_token;
  private $account_id;

  private $debug;
  private $emergency;
  private $warning;

  private $request;
  private $received;
  private $response;
  private $db;


  function __construct( $request, $db  )
  {
    $this->request = array();
    $this->account = array();

    $this->db = $db;
    $this->request = $request;

    $this->__include();

    // var_dump( $request ); die();
    $db_account = $this->fetch_account();

    $this->settings( $db_account );

    // print_r( $this->received ); die();
    $this->check_account();

    unset( $this->db );
    unset( $this->request );
    // var_dump( $this->account );
  }



  public function __get( $property )
  {
    if( property_exists( $this, $property ) )
    {
      return $this->$property;
    }
  }



  private function __include()
  {
    // Include the class that contains the JSON library
    require_once( dirname(__FILE__) . "/../lib/json.php" );
  }



  public function __set( $property, $value )
  {
    if( property_exists( $this, $property  ) )
    {
      $this->$property = $value;
    }

    return $this;
  }



  private function check_account()
  {
    // If the alias was set, but it is NOT in the account
    if( empty( $this->alias ) )
    {
      $this->emergency = "The alias was not found in the tradier.accounts table.";
    }
    // If there is no oauth_consumer_key in the database
    elseif( !$this->access_token || $this->access_token === NULL )
    {
      $this->emergency = "The access_token is missing from tradier.accounts table.";
    }
    // var_dump( $this->emergency ); die(lll);
  }



  function fetch_account()
  {
    // var_dump( $this->db ); die();
    // var_dump( $this->alias );
    // die();
    //  $values = array('alias' => 'teNO' );
    // $this->alias = 'testar';

    // Pull all of the information for the account
    list( $db_account ) = $this->db->GetAll("SELECT * FROM `accounts`
      WHERE `alias` = '" . $this->request['alias'] . "' LIMIT 1 " );

    $this->debug[] =  "account.php->fetch_account() = " . print_r( $db_account, TRUE ) . " ";

    return $db_account;
  }



  private function settings( $db_account )
  {
    if( !empty( $db_account ) )
    {
      if( isset( $db_account ) )
      {
        foreach( $db_account as $key => $value )
        {
          $this->$key = $value;
        }
      }
    }

    define('ACCESS_TOKEN', $this->access_token );
    define('ACCOUNT_ID', $this->account_id );
  }



}


?>
