<?php


class tradier
{
  private $account, $db, $debug, $request, $response;


  public function run_request( $request, $response, $db )
  {
    /*
     * CONCEPT:
     *
     * CAUTION:
     */

    // var_dump( $db); die();
    // var_dump( $response ); die();

    $this->includes();

    $this->db = $db;
    $this->request = $request;
    $this->response = $response;

    // var_dump( $this->db ); die();
    // var_dump( $this->request ); die();
    // var_dump( $this->response ); die(vvv);
    $this->account = new account( $this->request, $this->db );

    // var_dump( $this->account ); die(dddddd);

    // var_dump( $this->account->debug['warning'][0] ); die();
    $this->route_request();

    // Return the raw information. The information will be validated in jesus.php
    return $this->response;
  }



  private function debug_array( $array )
  {
    // Convert the array into a string . . .
    $str = print_r( $array, TRUE );

    // Remove all the annoying formatting that <pre> displays
    $str = str_replace( "\n ", '', $str );
    $str = str_replace( "\n", '', $str );
    $str = str_replace( "  ", '', $str );

    return $str;
  }



  private function includes()
  {
    require_once(dirname(__FILE__) . '/account.php');
    // require_once(dirname(__FILE__) . '/auth.php');
    require_once(dirname(__FILE__) . '/order.php');
    require_once(dirname(__FILE__) . '/status.php');
    require_once(dirname(__FILE__) . '/quotes.php');
    require_once(dirname(__FILE__) . '/../lib/json.php');

    // Initialize vars
    $this->request = array();
  }



  private function route_request()
  {
    /*
     * CONCEPT:
     * broker->testbroker->route_request() routes the request to the appropriate class and file.
     *
     * CAUTION:
     */

    // print_r( $this->request ); die();
    // print_r( $this->response ); die();

    $response = array();

    if( $this->account->emergency )
    {
      $this->response['emergency'] = $this->account->emergency;
      // var_dump( $this->response ); die();
    }
    elseif( $this->request['request'] === 'get broker status' )
    {
      $this->debug[] = 'shrine->route_request() get broker status';

      // Instantiate the class that gets the account's broker status
      $status = new status();

      $response = $status->get_status( $this->request );
      // $response = array('test' => 'here');
      // echo "ooooooooooooooooo"; die();
    }
    elseif( $this->request['request'] === 'get quotes' )
    {
      $this->debug[] = 'shrine->route_request() get quotes';

      // Instantiate the class that handles the quotes
      $quotes = new quotes();

      $response = $quotes->get_quotes( $this->request );
    }
    elseif( $this->request['request'] === 'order' )
    {
      $this->debug[] = 'shrine->route_request() order';

      // Instantiate the class that handles the orders
      $order = new order();

      $response = $order->execute( $this->request );
    }

    // echo 'eeee'; var_dump( $response ); die();
    // Merge the previously built response with the new information returned from the broker
    $this->response = array_merge( $this->response, $response );
    // echo 'eeee'; var_dump( $this->response ); die();
  }


}


?>
